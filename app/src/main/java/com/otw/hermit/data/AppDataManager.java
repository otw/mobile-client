package com.otw.hermit.data;

import com.otw.hermit.data.local.prefs.PreferencesHelper;
import com.otw.hermit.data.model.api.ConnectionsResponse;
import com.otw.hermit.data.model.api.ContactResponse;
import com.otw.hermit.data.model.api.UserRequest;
import com.otw.hermit.data.remote.ApiHeader;
import com.otw.hermit.data.remote.ApiHelper;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Single;

@Singleton
public class AppDataManager implements DataManager {

    private final ApiHelper mApiHelper;

    private final PreferencesHelper mPreferencesHelper;

    @Inject
    public AppDataManager(PreferencesHelper preferencesHelper, ApiHelper apiHelper) {
        mPreferencesHelper = preferencesHelper;
        mApiHelper = apiHelper;
    }

    @Override
    public Single<ContactResponse> serverLoginCall(UserRequest.ServerLoginRequest request) {
        return mApiHelper.serverLoginCall(request);
    }

    @Override
    public Single<ContactResponse> serverRegisterCall(UserRequest.ServerRegisterRequest request) {
        return mApiHelper.serverRegisterCall(request);
    }

    @Override
    public Single<ContactResponse> serverUpdateCall(UserRequest.UpdateRequest updateRequest) {
        return mApiHelper.serverUpdateCall(updateRequest);
    }

    @Override
    public Single<List<ConnectionsResponse.Contact>> serverConnectedContactsCall() {
        return mApiHelper.serverConnectedContactsCall();
    }

    @Override
    public Single<List<ConnectionsResponse.Event>> serverConnectedEventsCall() {
        return mApiHelper.serverConnectedEventsCall();
    }

    @Override
    public Single<List<ConnectionsResponse.Event>> serverEventsCall() {
        return mApiHelper.serverEventsCall();
    }

    @Override
    public Single<ConnectionsResponse.Contact> serverContactInfoCall(Long id) {
        return mApiHelper.serverContactInfoCall(id);
    }

    @Override
    public Single<ConnectionsResponse.Event> serverEventInfoCall(Long id) {
        return mApiHelper.serverEventInfoCall(id);
    }

    @Override
    public Single<ConnectionsResponse.Dossier> serverDossierCall(Long id) {
        return mApiHelper.serverDossierCall(id);
    }

    @Override
    public void makeConnectionToContact(Long id, ConnectionsResponse.File file) {
        mApiHelper.makeConnectionToContact(id, file);
    }


    @Override
    public void addFileToDossier(ConnectionsResponse.File file) {
        mApiHelper.addFileToDossier(file);
    }

    @Override
    public String getAccessToken() {
        return mPreferencesHelper.getAccessToken();
    }

    @Override
    public void setAccessToken(String accessToken) {
        mPreferencesHelper.setAccessToken(accessToken);
        mApiHelper.getApiHeader().getProtectedApiHeader().setToken(accessToken);
    }

    @Override
    public ApiHeader getApiHeader() {
        return mApiHelper.getApiHeader();
    }

    @Override
    public String getCurrentUserEmail() {
        return mPreferencesHelper.getCurrentUserEmail();
    }

    @Override
    public void setCurrentUserEmail(String email) {
        mPreferencesHelper.setCurrentUserEmail(email);
    }

    @Override
    public int getCurrentUserLoggedInMode() {
        return mPreferencesHelper.getCurrentUserLoggedInMode();
    }

    @Override
    public void setCurrentUserLoggedInMode(LoggedInMode mode) {
        mPreferencesHelper.setCurrentUserLoggedInMode(mode);
    }

    @Override
    public Long getCurrentUserId() {
        return mPreferencesHelper.getCurrentUserId();
    }

    @Override
    public void setCurrentUserId(Long userId) {
        mPreferencesHelper.setCurrentUserId(userId);
    }

    @Override
    public String getCurrentFirstName() {
        return mPreferencesHelper.getCurrentFirstName();
    }

    @Override
    public void setCurrentFirstName(String userName) {
        mPreferencesHelper.setCurrentFirstName(userName);
    }

    @Override
    public String getCurrentLastName() {
        return mPreferencesHelper.getCurrentLastName();
    }

    @Override
    public void setCurrentLastName(String lastName) {
        mPreferencesHelper.setCurrentLastName(lastName);
    }

    @Override
    public String getCurrentUserProfilePicUrl() {
        return mPreferencesHelper.getCurrentUserProfilePicUrl();
    }

    @Override
    public void setCurrentUserProfilePicUrl(String profilePicUrl) {
        mPreferencesHelper.setCurrentUserProfilePicUrl(profilePicUrl);
    }

    @Override
    public List<Long> getCurrentPresentEvents() {
        return mPreferencesHelper.getCurrentPresentEvents();
    }

    @Override
    public void setCurrentPresentEvents(List<Long> currentPresentEventIds) {
        mPreferencesHelper.setCurrentPresentEvents(currentPresentEventIds);
    }

    @Override
    public void clearCurrentEvents() {
        mPreferencesHelper.clearCurrentEvents();
    }

    @Override
    public void updateApiHeader(String accessToken) {
        mApiHelper.getApiHeader().getProtectedApiHeader().setToken(accessToken);
    }

    @Override
    public void updateUserInfo(
            LoggedInMode loggedInMode,
            String accessToken,
            Long userId,
            String userFirstName,
            String userLastName,
            String email,
            String profilePicPath) {

        setCurrentUserLoggedInMode(loggedInMode);
        setAccessToken(accessToken);
        setCurrentUserId(userId);
        setCurrentFirstName(userFirstName);
        setCurrentLastName(userLastName);
        setCurrentUserEmail(email);
        setCurrentUserProfilePicUrl(profilePicPath);

        updateApiHeader(accessToken);
    }
}
