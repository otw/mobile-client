package com.otw.hermit.data.model.api;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public final class ConnectionsResponse {

    public static class Contact {

        @Expose
        @SerializedName("id")
        private Long id;

        @Expose
        @SerializedName("email")
        private String email;

        @Expose
        @SerializedName("name")
        private String name;

        @Expose
        @SerializedName("firstName")
        private String firstName;

        @Expose
        @SerializedName("lastName")
        private String lastName;

        @Expose
        @SerializedName("phoneNumber")
        private String phoneNumber;

        @Expose
        @SerializedName("description")
        private String description;

        private String imageUrl;

        public Contact(Long id, String email, String firstName, String lastName, String phoneNumber, String description) {
            this.id = id;
            this.email = email;
            this.firstName = firstName;
            this.lastName = lastName;
            this.phoneNumber = phoneNumber;
            this.description = description;
        }

        public Contact() {
        }

        public String getImageUrl() {
            return imageUrl;
        }

        public void setImageUrl(String imageUrl) {
            this.imageUrl = imageUrl;
        }

        public Long getId() {
            return id;
        }

        public void setId(Long id) {
            this.id = id;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getFirstName() {
            return firstName;
        }

        public void setFirstName(String firstName) {
            this.firstName = firstName;
        }

        public String getLastName() {
            return lastName;
        }

        public void setLastName(String lastName) {
            this.lastName = lastName;
        }

        public String getPhoneNumber() {
            return phoneNumber;
        }

        public void setPhoneNumber(String phoneNumber) {
            this.phoneNumber = phoneNumber;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }
    }

    public static class Event {

        @Expose
        @SerializedName("id")
        private Long id;

        @Expose
        @SerializedName("title")
        private String title;

        @Expose
        @SerializedName("description")
        private String description;

        @Expose
        @SerializedName("latitude")
        private Double latitude;

        @Expose
        @SerializedName("longitude")
        private Double longitude;

        @Expose
        @SerializedName("address")
        private String address;

        @Expose
        @SerializedName("startDate")
        private String startDate;

        @Expose
        @SerializedName("endDate")
        private String endDate;

        @Expose
        @SerializedName("radius")
        private double radius;

        @Expose
        @SerializedName("organiserId")
        private Long organiserId;

        public Event(Long id, String title, String description, Double latitude, Double longitude, String address, String startDate, String endDate, double radius, Long organiserId) {
            this.id = id;
            this.title = title;
            this.description = description;
            this.latitude = latitude;
            this.longitude = longitude;
            this.address = address;
            this.startDate = startDate;
            this.endDate = endDate;
            this.radius = radius;
            this.organiserId = organiserId;
        }

        public Event() {
        }

        public Long getId() {
            return id;
        }

        public void setId(Long id) {
            this.id = id;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public Double getLatitude() {
            return latitude;
        }

        public void setLatitude(Double latitude) {
            this.latitude = latitude;
        }

        public Double getLongitude() {
            return longitude;
        }

        public void setLongitude(Double longitude) {
            this.longitude = longitude;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public String getStartDate() {
            return startDate;
        }

        public void setStartDate(String startDate) {
            this.startDate = startDate;
        }

        public String getEndDate() {
            return endDate;
        }

        public void setEndDate(String endDate) {
            this.endDate = endDate;
        }

        public double getRadius() {
            return radius;
        }

        public void setRadius(double radius) {
            this.radius = radius;
        }

        public Long getOrganiserId() {
            return organiserId;
        }

        public void setOrganiserId(Long organiserId) {
            this.organiserId = organiserId;
        }
    }


    public static class Dossier {

        @Expose
        @SerializedName("id")
        private Long id;

        @Expose
        @SerializedName("file")
        private List<File> file;

        public Dossier() {
        }

        public Dossier(Long id, List<File> file) {
            this.id = id;
            this.file = file;
        }

        public Long getId() {
            return id;
        }

        public void setId(Long id) {
            this.id = id;
        }

        public List<File> getFile() {
            return file;
        }

        public void setFile(List<File> file) {
            this.file = file;
        }
    }

    public static class File {
        @Expose
        @SerializedName("id")
        private Long id;

        @Expose
        @SerializedName("contactId")
        private Long contactId;

        @Expose
        @SerializedName("time")
        private String time;

        @Expose
        @SerializedName("latitude")
        private Double latitude;

        @Expose
        @SerializedName("longitude")
        private Double longitude;

        @Expose
        @SerializedName("title")
        private String title;

        @Expose
        @SerializedName("note")
        private String note;

        @Expose
        @SerializedName("eventId")
        private Long eventId;

        public File() {
        }

        public File(Long id, Long contactId, String time, Double latitude, Double longitude, String title, String note, Long eventId) {
            this.id = id;
            this.contactId = contactId;
            this.time = time;
            this.latitude = latitude;
            this.longitude = longitude;
            this.title = title;
            this.note = note;
            this.eventId = eventId;
        }

        public Long getId() {
            return id;
        }

        public void setId(Long id) {
            this.id = id;
        }

        public Long getContactId() {
            return contactId;
        }

        public void setContactId(Long contactId) {
            this.contactId = contactId;
        }

        public String getTime() {
            return time;
        }

        public void setTime(String time) {
            this.time = time;
        }

        public Double getLatitude() {
            return latitude;
        }

        public void setLatitude(Double latitude) {
            this.latitude = latitude;
        }

        public Double getLongitude() {
            return longitude;
        }

        public void setLongitude(Double longitude) {
            this.longitude = longitude;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getNote() {
            return note;
        }

        public void setNote(String note) {
            this.note = note;
        }

        public Long getEventId() {
            return eventId;
        }

        public void setEventId(Long eventId) {
            this.eventId = eventId;
        }
    }
}
