
package com.otw.hermit.data.remote;

import com.otw.hermit.BuildConfig;

public final class ApiEndPoint {

    public static final String ENDPOINT_SERVER_UPDATE = BuildConfig.BASE_URL + "/contact/contact/update";

    public static final String ENDPOINT_SERVER_LOGIN = BuildConfig.BASE_URL + "/contact/auth";

    public static final String ENDPOINT_SERVER_REGISTER = BuildConfig.BASE_URL + "/contact/contact/registration";

    public static final String ENDPOINT_SERVER_CREATE_CONNECTION = BuildConfig.BASE_URL + "/connection/connection/addConnection/";

    public static final String ENDPOINT_SERVER_CREATE_CONNECTION_OR_EVENT = BuildConfig.BASE_URL + "/connection/connection/addContactOrEventConnection/";

    public static final String ENDPOINT_SERVER_GET_CONTACTS = BuildConfig.BASE_URL + "/connection/connection/userConnectionsWithInfo";

    public static final String ENDPOINT_SERVER_GET_CONNECTED_EVENTS = BuildConfig.BASE_URL + "/connection/connection/userEventsWithInfo";

    public static final String ENDPOINT_SERVER_GET_EVENTS = BuildConfig.BASE_URL + "/event/event/getEvents";

    public static final String ENDPOINT_SERVER_GET_CONTACT_INFO = BuildConfig.BASE_URL + "/contact/contact/getContact/";

    public static final String ENDPOINT_SERVER_GET_EVENT_INFO = BuildConfig.BASE_URL + "/event/event/getEvent/";

    public static final String ENDPOINT_SERVER_ADD_FILE_DOSSIER = BuildConfig.BASE_URL + "/dossier/dossier/addFileToDossier";

    public static final String ENDPOINT_SERVER_GET_CONTACT_DOSSIER = BuildConfig.BASE_URL + "/dossier/dossier/getContactDossiers/";


    private ApiEndPoint() {
        // This class is not publicly instantiable
    }

}
