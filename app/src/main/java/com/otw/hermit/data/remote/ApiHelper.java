package com.otw.hermit.data.remote;


import com.otw.hermit.data.model.api.ConnectionsResponse;
import com.otw.hermit.data.model.api.ContactResponse;
import com.otw.hermit.data.model.api.UserRequest;

import java.util.List;

import io.reactivex.Single;

public interface ApiHelper {

    Single<ContactResponse> serverLoginCall(UserRequest.ServerLoginRequest request);

    Single<ContactResponse> serverRegisterCall(UserRequest.ServerRegisterRequest request);

    Single<ContactResponse> serverUpdateCall(UserRequest.UpdateRequest updateRequest);

    Single<List<ConnectionsResponse.Contact>> serverConnectedContactsCall();

    Single<List<ConnectionsResponse.Event>> serverConnectedEventsCall();

    Single<List<ConnectionsResponse.Event>> serverEventsCall();

    Single<ConnectionsResponse.Contact> serverContactInfoCall(Long id);

    Single<ConnectionsResponse.Event> serverEventInfoCall(Long id);

    Single<ConnectionsResponse.Dossier> serverDossierCall(Long id);

    void makeConnectionToContact(Long id, ConnectionsResponse.File file);

    void addFileToDossier(ConnectionsResponse.File file);



    ApiHeader getApiHeader();
}
