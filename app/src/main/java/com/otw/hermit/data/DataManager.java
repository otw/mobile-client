package com.otw.hermit.data;


import com.otw.hermit.data.local.prefs.PreferencesHelper;
import com.otw.hermit.data.remote.ApiHelper;

public interface DataManager extends PreferencesHelper, ApiHelper {

    void updateApiHeader(String accessToken);

    void updateUserInfo(
            LoggedInMode loggedInMode,
            String accessToken,
            Long userId,
            String userFirstName,
            String userLastName,
            String email,
            String profilePicPath);

    enum LoggedInMode {

        LOGGED_IN_MODE_LOGGED_OUT(0),
        LOGGED_IN_MODE_SERVER(1);

        private final int mType;

        LoggedInMode(int type) {
            mType = type;
        }

        public int getType() {
            return mType;
        }
    }
}
