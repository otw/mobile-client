package com.otw.hermit.data.local.prefs;

import android.content.Context;
import android.content.SharedPreferences;

import com.otw.hermit.data.DataManager;
import com.otw.hermit.utils.AppConstants;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import javax.inject.Inject;

public class AppPreferencesHelper implements PreferencesHelper {

    private static final String PREF_KEY_ACCESS_TOKEN = "PREF_KEY_ACCESS_TOKEN";

    private static final String PREF_KEY_CURRENT_USER_ID = "PREF_KEY_CURRENT_USER_ID";

    private static final String PREF_KEY_CURRENT_USER_FIRST_NAME = "PREF_KEY_CURRENT_USER_FIRST_NAME";

    private static final String PREF_KEY_CURRENT_USER_LAST_NAME = "PREF_KEY_CURRENT_USER_LAST_NAME";

    private static final String PREF_KEY_CURRENT_USER_EMAIL = "PREF_KEY_CURRENT_USER_EMAIL";

    private static final String PREF_KEY_CURRENT_USER_PROFILE_PIC_URL = "PREF_KEY_CURRENT_USER_PROFILE_PIC_URL";

    private static final String PREF_KEY_USER_LOGGED_IN_MODE = "PREF_KEY_USER_LOGGED_IN_MODE";

    private static final String PREF_KEY_CURRENT_PRESENT_EVENTS = "PREF_KEY_CURRENT_PRESENT_EVENTS";

    private final SharedPreferences mPrefs;

    @Inject
    public AppPreferencesHelper(Context context) {
        mPrefs = context.getSharedPreferences(AppConstants.PREF_NAME, Context.MODE_PRIVATE);
    }

    @Override
    public String getAccessToken() {
        return mPrefs.getString(PREF_KEY_ACCESS_TOKEN, null);
    }

    @Override
    public void setAccessToken(String accessToken) {
        mPrefs.edit().putString(PREF_KEY_ACCESS_TOKEN, accessToken).apply();
    }

    @Override
    public String getCurrentUserEmail() {
        return mPrefs.getString(PREF_KEY_CURRENT_USER_EMAIL, null);
    }

    @Override
    public void setCurrentUserEmail(String email) {
        mPrefs.edit().putString(PREF_KEY_CURRENT_USER_EMAIL, email).apply();
    }

    @Override
    public Long getCurrentUserId() {
        long userId = mPrefs.getLong(PREF_KEY_CURRENT_USER_ID, AppConstants.NULL_INDEX);
        return userId == AppConstants.NULL_INDEX ? null : userId;
    }

    @Override
    public void setCurrentUserId(Long userId) {
        long id = userId == null ? AppConstants.NULL_INDEX : userId;
        mPrefs.edit().putLong(PREF_KEY_CURRENT_USER_ID, id).apply();
    }

    @Override
    public String getCurrentFirstName() {
        return mPrefs.getString(PREF_KEY_CURRENT_USER_FIRST_NAME, null);
    }

    @Override
    public void setCurrentFirstName(String userName) {
        mPrefs.edit().putString(PREF_KEY_CURRENT_USER_FIRST_NAME, userName).apply();
    }

    @Override
    public String getCurrentLastName() {
        return mPrefs.getString(PREF_KEY_CURRENT_USER_LAST_NAME, null);
    }

    @Override
    public void setCurrentLastName(String lastName) {
        mPrefs.edit().putString(PREF_KEY_CURRENT_USER_LAST_NAME, lastName).apply();
    }

    @Override
    public int getCurrentUserLoggedInMode() {
        return mPrefs.getInt(PREF_KEY_USER_LOGGED_IN_MODE,
                DataManager.LoggedInMode.LOGGED_IN_MODE_LOGGED_OUT.getType());
    }

    @Override
    public void setCurrentUserLoggedInMode(DataManager.LoggedInMode mode) {
        mPrefs.edit().putInt(PREF_KEY_USER_LOGGED_IN_MODE, mode.getType()).apply();
    }

    @Override
    public String getCurrentUserProfilePicUrl() {
        return mPrefs.getString(PREF_KEY_CURRENT_USER_PROFILE_PIC_URL, null);
    }

    @Override
    public void setCurrentUserProfilePicUrl(String profilePicUrl) {
        mPrefs.edit().putString(PREF_KEY_CURRENT_USER_PROFILE_PIC_URL, profilePicUrl).apply();
    }

    @Override
    public List<Long> getCurrentPresentEvents() {
        Set<String> set = mPrefs.getStringSet(PREF_KEY_CURRENT_PRESENT_EVENTS, null);
        List<Long> longs = new LinkedList<>();
        if (set != null)
            new ArrayList<>(set).stream().map(o -> Long.parseLong(o)).forEach(longs::add);
        return longs;
    }

    @Override
    public void setCurrentPresentEvents(List<Long> currentPresentEvents) {
        List<String> strings = new LinkedList<>();
        currentPresentEvents.stream().map(o -> o.toString()).forEach(strings::add);
        mPrefs.edit().putStringSet(PREF_KEY_CURRENT_PRESENT_EVENTS, new HashSet<>(strings)).apply();
    }

    @Override
    public void clearCurrentEvents() {
        mPrefs.edit().putStringSet(PREF_KEY_CURRENT_PRESENT_EVENTS, null).apply();
    }
}
