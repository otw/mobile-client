package com.otw.hermit.data.local.prefs;


import com.otw.hermit.data.DataManager;

import java.util.List;

public interface PreferencesHelper {

    String getAccessToken();

    void setAccessToken(String accessToken);

    Long getCurrentUserId();

    void setCurrentUserId(Long userId);

    String getCurrentFirstName();

    void setCurrentFirstName(String firstName);

    String getCurrentLastName();

    void setCurrentLastName(String lastName);

    String getCurrentUserEmail();

    void setCurrentUserEmail(String email);

    int getCurrentUserLoggedInMode();

    void setCurrentUserLoggedInMode(DataManager.LoggedInMode mode);

    String getCurrentUserProfilePicUrl();

    void setCurrentUserProfilePicUrl(String profilePicUrl);

    List<Long> getCurrentPresentEvents();

    void setCurrentPresentEvents(List<Long> currentPresentEventTitles);

    void clearCurrentEvents();
}
