package com.otw.hermit.data.remote;


import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.OkHttpResponseListener;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.otw.hermit.data.model.api.ConnectionsResponse;
import com.otw.hermit.data.model.api.ContactResponse;
import com.otw.hermit.data.model.api.UserRequest;
import com.rx2androidnetworking.Rx2AndroidNetworking;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Single;
import okhttp3.Response;

@Singleton
public class AppApiHelper implements ApiHelper {

    private ApiHeader mApiHeader;

    @Inject
    ObjectMapper objectMapper;


    @Inject
    public AppApiHelper(ApiHeader apiHeader) {
        mApiHeader = apiHeader;
    }

    @Override
    public Single<ContactResponse> serverLoginCall(UserRequest.ServerLoginRequest loginRequest) {
        try {
            return Rx2AndroidNetworking.post(ApiEndPoint.ENDPOINT_SERVER_LOGIN)
                    .addJSONObjectBody(new JSONObject(objectMapper.writeValueAsString(loginRequest)))
                    .build()
                    .getObjectSingle(ContactResponse.class);
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public Single<ContactResponse> serverRegisterCall(UserRequest.ServerRegisterRequest request) {
        try {
            return Rx2AndroidNetworking.post(ApiEndPoint.ENDPOINT_SERVER_REGISTER)
                    .addJSONObjectBody(new JSONObject(objectMapper.writeValueAsString(request)))
                    .build()
                    .getObjectSingle(ContactResponse.class);
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public Single<ContactResponse> serverUpdateCall(UserRequest.UpdateRequest updateRequest) {
        try {
            return Rx2AndroidNetworking.put(ApiEndPoint.ENDPOINT_SERVER_UPDATE)
                    .addHeaders("Authorization", mApiHeader.getProtectedApiHeader().getToken())
                    .addJSONObjectBody(new JSONObject(objectMapper.writeValueAsString(updateRequest)))
                    .build()
                    .getObjectSingle(ContactResponse.class);
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public Single<List<ConnectionsResponse.Contact>> serverConnectedContactsCall() {
        return Rx2AndroidNetworking.get(ApiEndPoint.ENDPOINT_SERVER_GET_CONTACTS)
                .addHeaders("Authorization", mApiHeader.getProtectedApiHeader().getToken())
                .build()
                .getObjectListSingle(ConnectionsResponse.Contact.class);
    }

    @Override
    public Single<List<ConnectionsResponse.Event>> serverConnectedEventsCall() {
        return Rx2AndroidNetworking.get(ApiEndPoint.ENDPOINT_SERVER_GET_CONNECTED_EVENTS)
                .addHeaders("Authorization", mApiHeader.getProtectedApiHeader().getToken())
                .build()
                .getObjectListSingle(ConnectionsResponse.Event.class);
    }

    @Override
    public Single<List<ConnectionsResponse.Event>> serverEventsCall() {
        return Rx2AndroidNetworking.get(ApiEndPoint.ENDPOINT_SERVER_GET_EVENTS)
                .addHeaders("Authorization", mApiHeader.getProtectedApiHeader().getToken())
                .build()
                .getObjectListSingle(ConnectionsResponse.Event.class);
    }

    @Override
    public Single<ConnectionsResponse.Contact> serverContactInfoCall(Long id) {
        return Rx2AndroidNetworking.get(ApiEndPoint.ENDPOINT_SERVER_GET_CONTACT_INFO + id.toString())
                .addHeaders("Authorization", mApiHeader.getProtectedApiHeader().getToken())
                .build()
                .getObjectSingle(ConnectionsResponse.Contact.class);
    }

    @Override
    public Single<ConnectionsResponse.Event> serverEventInfoCall(Long id) {
        return Rx2AndroidNetworking.get(ApiEndPoint.ENDPOINT_SERVER_GET_EVENT_INFO + id.toString())
                .addHeaders("Authorization", mApiHeader.getProtectedApiHeader().getToken())
                .build()
                .getObjectSingle(ConnectionsResponse.Event.class);
    }

    @Override
    public Single<ConnectionsResponse.Dossier> serverDossierCall(Long id) {
        return Rx2AndroidNetworking.get(ApiEndPoint.ENDPOINT_SERVER_GET_CONTACT_DOSSIER + id.toString())
                .addHeaders("Authorization", mApiHeader.getProtectedApiHeader().getToken())
                .build()
                .getObjectSingle(ConnectionsResponse.Dossier.class);
    }

    @Override
    public void makeConnectionToContact(Long id, ConnectionsResponse.File file) {
        try {
            Rx2AndroidNetworking.put(ApiEndPoint.ENDPOINT_SERVER_CREATE_CONNECTION_OR_EVENT + id.toString())
                    .addHeaders("Authorization", mApiHeader.getProtectedApiHeader().getToken())
                    .addJSONObjectBody(new JSONObject(objectMapper.writeValueAsString(file)))
                    .build()
                    .getAsOkHttpResponse(new OkHttpResponseListener() {
                        @Override
                        public void onResponse(Response response) {
                            System.out.println("BAM" + response.code());
                        }

                        @Override
                        public void onError(ANError anError) {
                            System.out.println("BOEEEE" + anError.getErrorCode());
                        }
                    });
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void addFileToDossier(ConnectionsResponse.File file) {
        try {
            Rx2AndroidNetworking.put(ApiEndPoint.ENDPOINT_SERVER_ADD_FILE_DOSSIER)
                    .addHeaders("Authorization", mApiHeader.getProtectedApiHeader().getToken())
                    .addJSONObjectBody(new JSONObject(objectMapper.writeValueAsString(file)))
                    .build()
                    .getAsOkHttpResponse(new OkHttpResponseListener() {
                        @Override
                        public void onResponse(Response response) {
                            System.out.println("BAM" + response.code());
                        }

                        @Override
                        public void onError(ANError anError) {
                            System.out.println("BOEEEE" + anError.getErrorCode());
                        }
                    });
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
    }

    @Override
    public ApiHeader getApiHeader() {
        return mApiHeader;
    }
}
