package com.otw.hermit.di.builder;

import com.otw.hermit.ui.login.LoginActivity;
import com.otw.hermit.ui.login.LoginActivityModule;
import com.otw.hermit.ui.main.MainActivity;
import com.otw.hermit.ui.main.MainActivityModule;
import com.otw.hermit.ui.main.contact.ContactFragmentProvider;
import com.otw.hermit.ui.main.dossier.DossierFragmentProvider;
import com.otw.hermit.ui.main.event.EventFragmentProvider;
import com.otw.hermit.ui.main.potential.PotentialFragmentProvider;
import com.otw.hermit.ui.main.service.GeofenceTransitionService;
import com.otw.hermit.ui.main.service.ServiceModule;
import com.otw.hermit.ui.profile.ProfileActivity;
import com.otw.hermit.ui.profile.ProfileActivityModule;
import com.otw.hermit.ui.register.RegisterActivity;
import com.otw.hermit.ui.register.RegisterActivityModule;
import com.otw.hermit.ui.scanqr.ScanqrActivity;
import com.otw.hermit.ui.scanqr.ScanqrActivityModule;
import com.otw.hermit.ui.splash.SplashActivity;
import com.otw.hermit.ui.splash.SplashActivityModule;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class ActivityBuilder {

    @ContributesAndroidInjector(modules = ProfileActivityModule.class)
    abstract ProfileActivity bindProfileActivity();

    @ContributesAndroidInjector(modules = LoginActivityModule.class)
    abstract LoginActivity bindLoginActivity();

    @ContributesAndroidInjector(modules = ScanqrActivityModule.class)
    abstract ScanqrActivity bindScanQrActivity();

    @ContributesAndroidInjector(modules = RegisterActivityModule.class)
    abstract RegisterActivity bindRegisterActivity();

    @ContributesAndroidInjector(modules = ServiceModule.class)
    abstract GeofenceTransitionService bindGeofenceTransitionService();

    @ContributesAndroidInjector(modules = {
            MainActivityModule.class,
            ContactFragmentProvider.class,
            EventFragmentProvider.class,
            DossierFragmentProvider.class,
            PotentialFragmentProvider.class})
    abstract MainActivity bindMainActivity();

    @ContributesAndroidInjector(modules = SplashActivityModule.class)
    abstract SplashActivity bindSplashActivity();
}
