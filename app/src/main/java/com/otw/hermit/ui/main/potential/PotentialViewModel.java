package com.otw.hermit.ui.main.potential;

import android.arch.lifecycle.MutableLiveData;
import android.databinding.ObservableArrayList;
import android.databinding.ObservableList;

import com.otw.hermit.data.DataManager;
import com.otw.hermit.data.model.api.ConnectionsResponse;
import com.otw.hermit.ui.base.BaseViewModel;
import com.otw.hermit.utils.rx.SchedulerProvider;

import java.util.List;

public class PotentialViewModel extends BaseViewModel<PotentialNavigator> {

    public final ObservableList<ConnectionsResponse.Event> eventObservableArrayList = new ObservableArrayList<>();

    private final MutableLiveData<List<ConnectionsResponse.Event>> eventListLiveData;

    public PotentialViewModel(DataManager dataManager,
                              SchedulerProvider schedulerProvider) {
        super(dataManager, schedulerProvider);
        eventListLiveData = new MutableLiveData<>();
        fetchEvents();
    }

    public void addEventItemsToList(List<ConnectionsResponse.Event> events) {
        eventObservableArrayList.clear();
        eventObservableArrayList.addAll(events);
    }

    public void fetchEvents() {
        setIsLoading(true);
        getCompositeDisposable().add(getDataManager()
                .serverEventsCall()
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(response -> {
                    if (response != null && response.isEmpty() == false) {
                        eventListLiveData.setValue(response);
                    }
                    setIsLoading(false);
                }, throwable -> {
                    setIsLoading(false);
                    getNavigator().handleError(throwable);
                }));
    }

    public MutableLiveData<List<ConnectionsResponse.Event>> getEventListLiveData() {
        return eventListLiveData;
    }

    public ObservableList<ConnectionsResponse.Event> getEventObservableList() {
        return eventObservableArrayList;
    }
}
