package com.otw.hermit.ui.register;

public interface RegisterNavigator {

    void handleError(Throwable throwable);

    void register();

    void openMainActivity();

    void openLoginActivity();
}
