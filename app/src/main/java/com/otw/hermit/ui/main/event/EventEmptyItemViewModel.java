package com.otw.hermit.ui.main.event;

public class EventEmptyItemViewModel {

    private EventEmptyItemViewModelListener mListener;

    public EventEmptyItemViewModel(EventEmptyItemViewModelListener listener) {
        this.mListener = listener;
    }

    public void onRetryClick() {
        mListener.onRetryClick();
    }

    public interface EventEmptyItemViewModelListener {

        void onRetryClick();
    }
}
