package com.otw.hermit.ui.main.event;

import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.view.View;

import com.otw.hermit.BR;
import com.otw.hermit.R;
import com.otw.hermit.ViewModelProviderFactory;
import com.otw.hermit.data.model.api.ConnectionsResponse;
import com.otw.hermit.databinding.FragmentEventBinding;
import com.otw.hermit.ui.base.BaseFragment;

import java.util.List;

import javax.inject.Inject;

public class EventFragment extends BaseFragment<FragmentEventBinding, EventViewModel>
        implements EventNavigator, EventAdapter.EventAdapterListener {

    @Inject
    EventAdapter mEventAdapter;
    FragmentEventBinding mFragmentEventBinding;
    @Inject
    LinearLayoutManager mLayoutManager;
    @Inject
    ViewModelProviderFactory mViewModelFactory;
    private EventViewModel mEventViewModel;

    public static EventFragment newInstance() {
        Bundle args = new Bundle();
        EventFragment fragment = new EventFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public int getBindingVariable() {
        return BR.viewModel;
    }

    @Override
    public int getLayoutId() {
        return R.layout.fragment_event;
    }

    @Override
    public EventViewModel getViewModel() {
        mEventViewModel = ViewModelProviders.of(this, mViewModelFactory).get(EventViewModel.class);
        return mEventViewModel;
    }

    @Override
    public void handleError(Throwable throwable) {
        // handle error
    }

    @Override
    public void updateConnections(List<ConnectionsResponse.Event> eventList) {
        mEventAdapter.addItems(eventList);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mEventViewModel.setNavigator(this);
        mEventAdapter.setListener(this);
    }

    @Override
    public void onRetryClick() {
        mEventViewModel.fetchEvents();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mFragmentEventBinding = getViewDataBinding();
        setUp();
        subscribeToLiveData();
    }

    private void setUp() {
        mLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        mFragmentEventBinding.eventRecyclerView.setLayoutManager(mLayoutManager);
        mFragmentEventBinding.eventRecyclerView.setItemAnimator(new DefaultItemAnimator());
        mFragmentEventBinding.eventRecyclerView.setAdapter(mEventAdapter);
    }

    private void subscribeToLiveData() {
        mEventViewModel.getEventListLiveData().observe(this, events -> mEventViewModel.addEventItemsToList(events));
    }
}
