package com.otw.hermit.ui.main.service;

import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.app.TaskStackBuilder;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.google.android.gms.location.Geofence;
import com.google.android.gms.location.GeofenceStatusCodes;
import com.google.android.gms.location.GeofencingEvent;
import com.otw.hermit.R;
import com.otw.hermit.data.DataManager;
import com.otw.hermit.ui.main.MainActivity;
import com.otw.hermit.utils.rx.SchedulerProvider;

import java.util.List;

import javax.inject.Inject;

import dagger.android.AndroidInjection;
import dagger.android.AndroidInjector;
import dagger.android.DispatchingAndroidInjector;
import dagger.android.HasServiceInjector;
import io.reactivex.disposables.CompositeDisposable;

public class GeofenceTransitionService extends IntentService implements HasServiceInjector {

    public static final int GEOFENCE_NOTIFICATION_ID = 0;
    private static final String TAG = GeofenceTransitionService.class.getSimpleName();

    @Inject
    DispatchingAndroidInjector<Service> serviceDispatchingAndroidInjector;

    @Inject
    DataManager dataManager;

    @Inject
    SchedulerProvider schedulerProvider;

    public GeofenceTransitionService() {
        super(TAG);
    }

    public GeofenceTransitionService(DataManager dataManager, SchedulerProvider schedulerProvider) {
        super(TAG);
        this.dataManager = dataManager;
        this.schedulerProvider = schedulerProvider;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        AndroidInjection.inject(this);
    }

    private static String getErrorString(int errorCode) {
        switch (errorCode) {
            case GeofenceStatusCodes.GEOFENCE_NOT_AVAILABLE:
                return "GeoFence not available";
            case GeofenceStatusCodes.GEOFENCE_TOO_MANY_GEOFENCES:
                return "Too many GeoFences";
            case GeofenceStatusCodes.GEOFENCE_TOO_MANY_PENDING_INTENTS:
                return "Too many pending intents";
            default:
                return "Unknown error.";
        }
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        GeofencingEvent geofencingEvent = GeofencingEvent.fromIntent(intent);

        // Handling errors
        if (geofencingEvent.hasError()) {
            String errorMsg = getErrorString(geofencingEvent.getErrorCode());
            Log.e(TAG, errorMsg);
            return;
        }

        int geoFenceTransition = geofencingEvent.getGeofenceTransition();

        // Check if the transition type is of interest
        if (geoFenceTransition == Geofence.GEOFENCE_TRANSITION_ENTER ||
                geoFenceTransition == Geofence.GEOFENCE_TRANSITION_EXIT) {
            for (Geofence triggeredGeofence : geofencingEvent.getTriggeringGeofences()) {
                if (addToPrefs(geoFenceTransition, triggeredGeofence)) {
                    getEventInfo(Long.parseLong(triggeredGeofence.getRequestId()), geoFenceTransition);
                }
            }
        }
    }

    private boolean addToPrefs(int geoFenceTransition, Geofence geofence) {
        List<Long> currentEvents = dataManager.getCurrentPresentEvents();

        if (!currentEvents.contains(Long.parseLong(geofence.getRequestId())) && geoFenceTransition == Geofence.GEOFENCE_TRANSITION_ENTER) {
            currentEvents.add(Long.parseLong(geofence.getRequestId()));
            dataManager.setCurrentPresentEvents(currentEvents);
            return true;
        } else if (currentEvents.contains(Long.parseLong(geofence.getRequestId())) && geoFenceTransition == Geofence.GEOFENCE_TRANSITION_EXIT) {
            currentEvents.remove(Long.parseLong(geofence.getRequestId()));
            dataManager.setCurrentPresentEvents(currentEvents);
            return true;
        }

        return false;
    }

    private String getGeofenceTrasitionDetails(int geoFenceTransition, String title) {
        String status = null;
        if (geoFenceTransition == Geofence.GEOFENCE_TRANSITION_ENTER)
            status = "Entering ";
        else if (geoFenceTransition == Geofence.GEOFENCE_TRANSITION_EXIT)
            status = "Exiting ";
        return status + title;
    }

    private void sendNotification(String msg) {
        // Intent to start the main Activity
        Intent notificationIntent = MainActivity.makeNotificationIntent(getApplicationContext(), msg);

        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
        stackBuilder.addParentStack(MainActivity.class);
        stackBuilder.addNextIntent(notificationIntent);
        PendingIntent notificationPendingIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);

        // Creating and sending Notification
        NotificationManager notificatioMng = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        notificatioMng.notify(GEOFENCE_NOTIFICATION_ID, createNotification(msg, notificationPendingIntent));
    }

    // Create notification
    private Notification createNotification(String msg, PendingIntent notificationPendingIntent) {
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this, "KAK2");
        notificationBuilder
                .setSmallIcon(R.drawable.ic_scan_qr)
                .setColor(Color.RED)
                .setContentTitle(msg)
                .setContentText("Geofence Notification!")
                .setContentIntent(notificationPendingIntent)
                .setDefaults(Notification.DEFAULT_LIGHTS | Notification.DEFAULT_VIBRATE | Notification.DEFAULT_SOUND)
                .setAutoCancel(true);
        return notificationBuilder.build();
    }

    public void getEventInfo(Long id, int geoFenceTransition) {
        System.out.println("START OPHALEN DATA");
        new CompositeDisposable().add(dataManager
                .serverEventInfoCall(id)
                .subscribeOn(schedulerProvider.io())
                .observeOn(schedulerProvider.ui())
                .subscribe(response -> {
                    if (response != null) {
                        System.out.println("DATA IS OPGEHAALD");
                        String geofenceTransitionDetails = getGeofenceTrasitionDetails(geoFenceTransition, response.getTitle());
                        // Send notification for each event
                        sendNotification(geofenceTransitionDetails);
                    }
                }, throwable -> {
                }));
    }


    @Override
    public AndroidInjector<Service> serviceInjector() {
        return serviceDispatchingAndroidInjector;
    }
}
