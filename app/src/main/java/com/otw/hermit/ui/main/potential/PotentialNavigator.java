package com.otw.hermit.ui.main.potential;

import com.otw.hermit.data.model.api.ConnectionsResponse;

import java.util.List;

public interface PotentialNavigator {

    void handleError(Throwable throwable);

    void updateConnections(List<ConnectionsResponse.Event> eventList);
}
