package com.otw.hermit.ui.main.contact;

import android.databinding.ObservableField;

import com.otw.hermit.data.model.api.ConnectionsResponse;

public class ContactItemViewModel {

    public final ObservableField<Long> id;

    public final ObservableField<String> name;

    public final ObservableField<String> lastName;

    public final ObservableField<String> phoneNumber;

    public final ObservableField<String> email;

    public final ObservableField<String> description;

    public final ContactItemViewModelListener mListener;

    private final ConnectionsResponse.Contact mContact;

    public ContactItemViewModel(ConnectionsResponse.Contact mContact, ContactItemViewModelListener listener) {
        this.mContact = mContact;
        this.mListener = listener;
        id = new ObservableField<>(this.mContact.getId());
        name = new ObservableField<>(this.mContact.getFirstName() + " " + this.mContact.getLastName());
        lastName = new ObservableField<>(this.mContact.getLastName());
        phoneNumber = new ObservableField<>(this.mContact.getPhoneNumber());
        email = new ObservableField<>(this.mContact.getEmail());
        description = new ObservableField<>(this.mContact.getDescription());
    }

    public void onItemClick() {
        mListener.onItemClick(mContact);
    }

    public interface ContactItemViewModelListener {

        void onItemClick(ConnectionsResponse.Contact contact);
    }
}
