package com.otw.hermit.ui.main;

import com.otw.hermit.data.DataManager;
import com.otw.hermit.ui.base.BaseViewModel;
import com.otw.hermit.utils.rx.SchedulerProvider;


public class MainViewModel extends BaseViewModel<MainNavigator> {

    public MainViewModel(DataManager dataManager, SchedulerProvider schedulerProvider) {
        super(dataManager, schedulerProvider);
    }

    public void scanQr() {
        getNavigator().openScanQrActivity();
    }

    public void openProfile() {
        getNavigator().openProfileActivity();
    }

    public void onEventButtonClick() {
        getNavigator().onEventButtonClick();
    }

    public void fetchEvents() {
        getCompositeDisposable().add(getDataManager()
                .serverEventsCall()
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(response -> {
                    if (response != null && response.isEmpty() == false) {
                        getNavigator().startGeofence(response);
                    }
                }, throwable -> {
                    getNavigator().handleError(throwable);
                }));
    }

    public void emptyPrefs() {
        getDataManager().clearCurrentEvents();
    }

    public boolean isGeofenceEmpty() {
        if (getDataManager().getCurrentPresentEvents().isEmpty()) return true;
        return false;
    }
}
