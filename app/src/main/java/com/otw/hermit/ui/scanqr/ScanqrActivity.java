package com.otw.hermit.ui.scanqr;

import android.Manifest;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Point;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.util.SparseArray;
import android.view.Display;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.widget.Toast;

import com.google.android.gms.vision.CameraSource;
import com.google.android.gms.vision.Detector;
import com.google.android.gms.vision.barcode.Barcode;
import com.google.android.gms.vision.barcode.BarcodeDetector;
import com.otw.hermit.BR;
import com.otw.hermit.R;
import com.otw.hermit.ViewModelProviderFactory;
import com.otw.hermit.data.model.api.ConnectionsResponse;
import com.otw.hermit.databinding.ActivityScanQrBinding;
import com.otw.hermit.ui.base.BaseActivity;
import com.otw.hermit.ui.main.MainActivity;
import com.otw.hermit.ui.scanqr.scannedcontact.ScannedContactAdapter;

import java.io.IOException;

import javax.inject.Inject;

public class ScanqrActivity extends BaseActivity<ActivityScanQrBinding, ScanqrViewModel> implements ScanqrNavigator, ScannedContactAdapter.ScannedContactAdapterListener {

    @Inject
    ViewModelProviderFactory mViewModelFactory;

    @Inject
    ScannedContactAdapter mScannedContactAdapter;

    @Inject
    LinearLayoutManager mLayoutManager;

    private ActivityScanQrBinding mActivityScanqrBinding;
    private ScanqrViewModel mScanqrViewModel;

    private SurfaceView cameraView;
    private BarcodeDetector barcode;
    private CameraSource cameraSource;
    private SurfaceHolder holder;

    public static Intent newIntent(Context context) {
        Intent intent = new Intent(context, ScanqrActivity.class);
        return intent;
    }

    @Override
    public int getBindingVariable() {
        return BR.viewModel;
    }

    @Override
    public int getLayoutId() {
        return R.layout.activity_scan_qr;
    }

    @Override
    public ScanqrViewModel getViewModel() {
        mScanqrViewModel = ViewModelProviders.of(this, mViewModelFactory).get(ScanqrViewModel.class);
        return mScanqrViewModel;
    }

    @Override
    public void handleError(Throwable throwable) {
        // handle error
    }

    @Override
    public void openMainActivity() {
        startActivity(MainActivity.newIntent(this));
        finish();
    }

    @Override
    public void onBackPressed() {
        mScanqrViewModel.openMainActivity();
    }

    public void onFragmentDetached(String tag) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        Fragment fragment = fragmentManager.findFragmentByTag(tag);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mActivityScanqrBinding = getViewDataBinding();
        mScannedContactAdapter.setListener(this);
        mScanqrViewModel.setNavigator(this);
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA}, 200);
        }
        setUp();
        setUpCamera();
        subscribeToLiveData();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }


    private void setUp() {
        mLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        mActivityScanqrBinding.scannedContactRecyclerView.setLayoutManager(mLayoutManager);
        mActivityScanqrBinding.scannedContactRecyclerView.setItemAnimator(new DefaultItemAnimator());
        mActivityScanqrBinding.scannedContactRecyclerView.setAdapter(mScannedContactAdapter);
    }


    private void setUpCamera() {
        cameraView = mActivityScanqrBinding.cameraView;
        cameraView.setZOrderMediaOverlay(true);
        holder = cameraView.getHolder();
        barcode = new BarcodeDetector.Builder(this)
                .setBarcodeFormats(Barcode.QR_CODE)
                .build();

        if (!barcode.isOperational()) {
            Toast.makeText(getApplicationContext(), "Sorry, Couldn't setup the detector", Toast.LENGTH_LONG).show();
            this.finish();
        }

        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);

        cameraSource = new CameraSource.Builder(this, barcode)
                .setFacing(CameraSource.CAMERA_FACING_BACK)
                .setRequestedFps(24)
                .setAutoFocusEnabled(true)
                .setRequestedPreviewSize(size.y, size.x)
                .build();

        cameraView.getHolder().addCallback(new SurfaceHolder.Callback() {
            @Override
            public void surfaceCreated(SurfaceHolder holder) {
                try {
                    if (ContextCompat.checkSelfPermission(ScanqrActivity.this, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {
                        cameraSource.start(cameraView.getHolder());
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {

            }

            @Override
            public void surfaceDestroyed(SurfaceHolder holder) {

            }
        });


        barcode.setProcessor(new Detector.Processor<Barcode>() {
            @Override
            public void release() {

            }

            @Override
            public void receiveDetections(Detector.Detections<Barcode> detections) {
                final SparseArray<Barcode> barcodes = detections.getDetectedItems();
                mScanqrViewModel.scannedCode(barcodes.valueAt(0).displayValue);
            }
        });
    }

    private void subscribeToLiveData() {
        mScanqrViewModel.getContactListLiveData().observe(this, contacts -> mScanqrViewModel.addContactItemsToList(contacts));
    }

    @Override
    public void onConnect(ConnectionsResponse.Contact contact) {
        mScanqrViewModel.connectToContact(contact.getId());
        System.out.println("make connnection to contact = " + contact);
    }

    @Override
    public void onDontConnect(ConnectionsResponse.Contact contact) {
        mScanqrViewModel.dontConnectToContact(contact);
        System.out.println("dont connect" + contact.getLastName());
    }
}
