package com.otw.hermit.ui.profile;

import android.text.TextUtils;

import com.otw.hermit.data.DataManager;
import com.otw.hermit.data.model.api.UserRequest;
import com.otw.hermit.ui.base.BaseViewModel;
import com.otw.hermit.utils.CommonUtils;
import com.otw.hermit.utils.rx.SchedulerProvider;

public class ProfileViewModel extends BaseViewModel<ProfileNavigator> {

    public ProfileViewModel(DataManager dataManager, SchedulerProvider schedulerProvider) {
        super(dataManager, schedulerProvider);
    }

    public boolean isFirstnameLastnameAndEmailValid(String email, String firstname, String lastname) {
        if (TextUtils.isEmpty(email)) {
            return false;
        }
        if (!CommonUtils.isEmailValid(email)) {
            return false;
        }
        return !TextUtils.isEmpty(firstname) || !TextUtils.isEmpty(lastname);
    }

    public Long getUserId() {
        return getDataManager().getCurrentUserId();
    }


    public String getFirstName() {
        return getDataManager().getCurrentFirstName();
    }

    public String getLastName() {
        return getDataManager().getCurrentLastName();
    }

    public String getEmail() {
        return getDataManager().getCurrentUserEmail();
    }

    public void openMainActivity() {
        getNavigator().openMainActivity();
    }

    public void saveProfile(String firstname, String lastname, String email, String phoneNumber, String description) {
        setIsLoading(true);
        getCompositeDisposable().add(getDataManager()
                .serverUpdateCall(new UserRequest.UpdateRequest(firstname, lastname, email, phoneNumber, description))
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(response -> {
                    setIsLoading(false);
                    getNavigator().openMainActivity();
                }, throwable -> {
                    setIsLoading(false);
                    getNavigator().handleError(throwable);
                }));
    }

    public void logout() {
        setIsLoading(true);
        getDataManager().updateUserInfo(
                DataManager.LoggedInMode.LOGGED_IN_MODE_LOGGED_OUT,
                null,
                null,
                null,
                null,
                null,
                null);
        getNavigator().openLoginActivity();
    }

    public void onToLoginClick() {
        getNavigator().openLoginActivity();
    }
}
