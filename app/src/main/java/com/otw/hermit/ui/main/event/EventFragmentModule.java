package com.otw.hermit.ui.main.event;

import android.support.v7.widget.LinearLayoutManager;

import com.otw.hermit.data.DataManager;
import com.otw.hermit.utils.rx.SchedulerProvider;

import java.util.ArrayList;

import dagger.Module;
import dagger.Provides;

@Module
public class EventFragmentModule {

    @Provides
    EventViewModel eventViewModel(DataManager dataManager,
                                  SchedulerProvider schedulerProvider) {
        return new EventViewModel(dataManager, schedulerProvider);
    }

    @Provides
    EventAdapter provideEventAdapter() {
        return new EventAdapter(new ArrayList<>());
    }

//    @Provides
//    ViewModelProvider.Factory provideBlogViewModel(ScannedContactViewModel eventViewModel) {
//        return new ViewModelProviderFactory<>(eventViewModel);
//    }

    @Provides
    LinearLayoutManager provideLinearLayoutManager(EventFragment fragment) {
        return new LinearLayoutManager(fragment.getActivity());
    }
}
