package com.otw.hermit.ui.main.potential;

import android.support.v7.widget.LinearLayoutManager;

import com.otw.hermit.data.DataManager;
import com.otw.hermit.utils.rx.SchedulerProvider;

import java.util.ArrayList;

import dagger.Module;
import dagger.Provides;

@Module
public class PotentialFragmentModule {

    @Provides
    PotentialViewModel eventViewModel(DataManager dataManager,
                                      SchedulerProvider schedulerProvider) {
        return new PotentialViewModel(dataManager, schedulerProvider);
    }

    @Provides
    PotentialAdapter provideEventAdapter() {
        return new PotentialAdapter(new ArrayList<>());
    }

//    @Provides
//    ViewModelProvider.Factory provideBlogViewModel(ScannedContactViewModel eventViewModel) {
//        return new ViewModelProviderFactory<>(eventViewModel);
//    }

    @Provides
    LinearLayoutManager provideLinearLayoutManager(PotentialFragment fragment) {
        return new LinearLayoutManager(fragment.getActivity());
    }
}
