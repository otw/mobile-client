package com.otw.hermit.ui.splash;


import com.otw.hermit.data.DataManager;
import com.otw.hermit.ui.main.contact.ContactAdapter;
import com.otw.hermit.ui.scanqr.scannedcontact.ScannedContactAdapter;
import com.otw.hermit.utils.rx.SchedulerProvider;

import java.util.ArrayList;

import dagger.Module;
import dagger.Provides;

@Module
public class SplashActivityModule {

    @Provides
    SplashViewModel provideSplashViewModel(DataManager dataManager, SchedulerProvider schedulerProvider) {
        return new SplashViewModel(dataManager, schedulerProvider);
    }


}
