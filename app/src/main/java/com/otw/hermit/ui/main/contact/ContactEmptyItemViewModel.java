package com.otw.hermit.ui.main.contact;

public class ContactEmptyItemViewModel {

    private ContactEmptyItemViewModelListener mListener;

    public ContactEmptyItemViewModel(ContactEmptyItemViewModelListener listener) {
        this.mListener = listener;
    }

    public void onRetryClick() {
        mListener.onRetryClick();
    }

    public interface ContactEmptyItemViewModelListener {

        void onRetryClick();
    }
}
