package com.otw.hermit.ui.profile;

public interface ProfileNavigator {

    void handleError(Throwable throwable);

    void saveProfile();

    void logout();

    void openMainActivity();

    void openLoginActivity();
}
