package com.otw.hermit.ui.main;

import com.otw.hermit.data.model.api.ConnectionsResponse;

import java.util.List;

public interface MainNavigator {

    void handleError(Throwable throwable);

    void openScanQrActivity();

    void openProfileActivity();

    void onEventButtonClick();

    void startGeofence(List<ConnectionsResponse.Event> events);
}
