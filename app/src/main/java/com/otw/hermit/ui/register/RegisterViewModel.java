package com.otw.hermit.ui.register;

import android.text.TextUtils;

import com.otw.hermit.data.DataManager;
import com.otw.hermit.data.model.api.UserRequest;
import com.otw.hermit.ui.base.BaseViewModel;
import com.otw.hermit.utils.CommonUtils;
import com.otw.hermit.utils.rx.SchedulerProvider;

public class RegisterViewModel extends BaseViewModel<RegisterNavigator> {

    public RegisterViewModel(DataManager dataManager, SchedulerProvider schedulerProvider) {
        super(dataManager, schedulerProvider);
    }

    public boolean isFirstnameLastnameEmailAndPasswordValid(String firstname, String lastname, String email, String password) {
        // validate email and password
        if (TextUtils.isEmpty(email)) {
            return false;
        }
        if (!CommonUtils.isEmailValid(email)) {
            return false;
        }
        return !TextUtils.isEmpty(password);
    }

    public void register(String firstname, String lastname, String email, String password) {
        setIsLoading(true);
        getCompositeDisposable().add(getDataManager()
                .serverRegisterCall(new UserRequest.ServerRegisterRequest(firstname, lastname, email, password))
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(response -> {
                    setIsLoading(false);
                    getNavigator().openLoginActivity();
                }, throwable -> {
                    setIsLoading(false);
                    getNavigator().handleError(throwable);
                }));
    }


    public void onServerRegisterClick() {
        getNavigator().register();
    }

    public void onToLoginClick() {
        getNavigator().openLoginActivity();
    }
}
