package com.otw.hermit.ui.main.potential;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class PotentialFragmentProvider {

    @ContributesAndroidInjector(modules = PotentialFragmentModule.class)
    abstract PotentialFragment provideEventFragmentFactory();
}
