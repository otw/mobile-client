package com.otw.hermit.ui.splash;


public interface SplashNavigator {

    void openLoginActivity();

    void openMainActivity();
}
