package com.otw.hermit.ui.scanqr;

import android.support.v7.widget.LinearLayoutManager;

import com.otw.hermit.ui.scanqr.scannedcontact.ScannedContactAdapter;

import java.util.ArrayList;

import dagger.Module;
import dagger.Provides;

@Module
public class ScanqrActivityModule {

    @Provides
    ScannedContactAdapter provideScannedContactAdapter() {
        return new ScannedContactAdapter(new ArrayList<>());
    }

    @Provides
    LinearLayoutManager provideLinearLayoutManager(ScanqrActivity activity) {
        return new LinearLayoutManager(activity);
    }
}
