package com.otw.hermit.ui.main.potential;

import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.view.View;

import com.otw.hermit.BR;
import com.otw.hermit.R;
import com.otw.hermit.ViewModelProviderFactory;
import com.otw.hermit.data.model.api.ConnectionsResponse;
import com.otw.hermit.databinding.FragmentPotentialBinding;
import com.otw.hermit.ui.base.BaseFragment;

import java.util.List;

import javax.inject.Inject;

public class PotentialFragment extends BaseFragment<FragmentPotentialBinding, PotentialViewModel>
        implements PotentialNavigator, PotentialAdapter.EventAdapterListener {

    @Inject
    PotentialAdapter mPotentialAdapter;
    FragmentPotentialBinding mFragmentPotentialBinding;
    @Inject
    LinearLayoutManager mLayoutManager;
    @Inject
    ViewModelProviderFactory mViewModelFactory;
    private PotentialViewModel mPotentialViewModel;

    public static PotentialFragment newInstance() {
        Bundle args = new Bundle();
        PotentialFragment fragment = new PotentialFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public int getBindingVariable() {
        return BR.viewModel;
    }

    @Override
    public int getLayoutId() {
        return R.layout.fragment_potential;
    }

    @Override
    public PotentialViewModel getViewModel() {
        mPotentialViewModel = ViewModelProviders.of(this, mViewModelFactory).get(PotentialViewModel.class);
        return mPotentialViewModel;
    }

    @Override
    public void handleError(Throwable throwable) {
        // handle error
    }

    @Override
    public void updateConnections(List<ConnectionsResponse.Event> eventList) {
        mPotentialAdapter.addItems(eventList);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mPotentialViewModel.setNavigator(this);
        mPotentialAdapter.setListener(this);
    }

    @Override
    public void onRetryClick() {
        mPotentialViewModel.fetchEvents();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mFragmentPotentialBinding = getViewDataBinding();
        setUp();
        subscribeToLiveData();
    }

    private void setUp() {
        mLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        mFragmentPotentialBinding.eventRecyclerView.setLayoutManager(mLayoutManager);
        mFragmentPotentialBinding.eventRecyclerView.setItemAnimator(new DefaultItemAnimator());
        mFragmentPotentialBinding.eventRecyclerView.setAdapter(mPotentialAdapter);
    }

    private void subscribeToLiveData() {
        mPotentialViewModel.getEventListLiveData().observe(this, events -> mPotentialViewModel.addEventItemsToList(events));
    }
}
