package com.otw.hermit.ui.register;

import com.otw.hermit.data.DataManager;
import com.otw.hermit.utils.rx.SchedulerProvider;

import dagger.Module;
import dagger.Provides;

@Module
public class RegisterActivityModule {

    @Provides
    RegisterViewModel provideLoginViewModel(DataManager dataManager, SchedulerProvider schedulerProvider) {
        return new RegisterViewModel(dataManager, schedulerProvider);
    }
}
