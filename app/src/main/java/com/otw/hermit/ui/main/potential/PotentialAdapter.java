package com.otw.hermit.ui.main.potential;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.otw.hermit.data.model.api.ConnectionsResponse;
import com.otw.hermit.databinding.ItemPotentialEmptyViewBinding;
import com.otw.hermit.databinding.ItemPotentialViewBinding;
import com.otw.hermit.ui.base.BaseViewHolder;

import java.util.List;

public class PotentialAdapter extends RecyclerView.Adapter<BaseViewHolder> {

    public static final int VIEW_TYPE_EMPTY = 0;

    public static final int VIEW_TYPE_NORMAL = 1;

    private List<ConnectionsResponse.Event> mEventResponse;

    private EventAdapterListener mListener;

    public PotentialAdapter(List<ConnectionsResponse.Event> eventList) {
        this.mEventResponse = eventList;
    }

    @Override
    public int getItemCount() {
        if (mEventResponse != null && mEventResponse.size() > 0) {
            return mEventResponse.size();
        } else {
            return 1;
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (mEventResponse != null && !mEventResponse.isEmpty()) {
            return VIEW_TYPE_NORMAL;
        } else {
            return VIEW_TYPE_EMPTY;
        }
    }

    @Override
    public void onBindViewHolder(BaseViewHolder holder, int position) {
        holder.onBind(position);
    }

    @Override
    public BaseViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        switch (viewType) {
            case VIEW_TYPE_NORMAL:
                ItemPotentialViewBinding itemViewBinding = ItemPotentialViewBinding.inflate(LayoutInflater.from(parent.getContext()),
                        parent, false);
                return new ConnectionViewHolder(itemViewBinding);
            case VIEW_TYPE_EMPTY:
            default:
                ItemPotentialEmptyViewBinding emptyViewBinding = ItemPotentialEmptyViewBinding.inflate(LayoutInflater.from(parent.getContext()),
                        parent, false);
                return new EmptyViewHolder(emptyViewBinding);
        }
    }

    public void addItems(List<ConnectionsResponse.Event> responseList) {
        mEventResponse.addAll(responseList);
        notifyDataSetChanged();
    }

    public void clearItems() {
        mEventResponse.clear();
    }

    public void setListener(EventAdapterListener listener) {
        this.mListener = listener;
    }

    public interface EventAdapterListener {

        void onRetryClick();
    }

    public class ConnectionViewHolder extends BaseViewHolder implements PotentialItemViewModel.EventItemViewModelListener {

        private ItemPotentialViewBinding mBinding;

        private PotentialItemViewModel mPotentialItemViewModel;

        public ConnectionViewHolder(ItemPotentialViewBinding binding) {
            super(binding.getRoot());
            this.mBinding = binding;
        }

        @Override
        public void onBind(int position) {
            final ConnectionsResponse.Event event = mEventResponse.get(position);
            mPotentialItemViewModel = new PotentialItemViewModel(event, this);
            mBinding.setViewModel(mPotentialItemViewModel);

            // Immediate Binding
            // When a variable or observable changes, the binding will be scheduled to change before
            // the next frame. There are times, however, when binding must be executed immediately.
            // To force execution, use the executePendingBindings() method.
            mBinding.executePendingBindings();
        }

        @Override
        public void onItemClick(Long contactId) {

        }
    }

    public class EmptyViewHolder extends BaseViewHolder implements PotentialEmptyItemViewModel.EventEmptyItemViewModelListener {

        private ItemPotentialEmptyViewBinding mBinding;

        public EmptyViewHolder(ItemPotentialEmptyViewBinding binding) {
            super(binding.getRoot());
            this.mBinding = binding;
        }

        @Override
        public void onBind(int position) {
            PotentialEmptyItemViewModel emptyItemViewModel = new PotentialEmptyItemViewModel(this);
            mBinding.setViewModel(emptyItemViewModel);
        }

        @Override
        public void onRetryClick() {
            mListener.onRetryClick();
        }
    }
}