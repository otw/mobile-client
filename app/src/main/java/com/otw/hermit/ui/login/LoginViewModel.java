package com.otw.hermit.ui.login;

import android.text.TextUtils;

import com.otw.hermit.data.DataManager;
import com.otw.hermit.data.model.api.UserRequest;
import com.otw.hermit.ui.base.BaseViewModel;
import com.otw.hermit.utils.CommonUtils;
import com.otw.hermit.utils.rx.SchedulerProvider;

public class LoginViewModel extends BaseViewModel<LoginNavigator> {

    public LoginViewModel(DataManager dataManager, SchedulerProvider schedulerProvider) {
        super(dataManager, schedulerProvider);
    }

    public boolean isEmailAndPasswordValid(String email, String password) {
        // validate email and password
        if (TextUtils.isEmpty(email)) {
            return false;
        }
        if (!CommonUtils.isEmailValid(email)) {
            return false;
        }
        return !TextUtils.isEmpty(password);
    }

    public void login(String email, String password) {
        setIsLoading(true);
        getCompositeDisposable().add(getDataManager().serverLoginCall(new UserRequest.ServerLoginRequest(email, password))
                .doOnSuccess(response -> getDataManager()
                        .updateUserInfo(
                                DataManager.LoggedInMode.LOGGED_IN_MODE_SERVER,
                                response.getAccessToken(),
                                response.getId(),
                                response.getFirstName(),
                                response.getLastName(),
                                response.getEmail(),
                                null))
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(response -> {
                    setIsLoading(false);
                    getNavigator().openMainActivity();
                }, throwable -> {
                    setIsLoading(false);
                    getNavigator().handleError(throwable);
                }));
    }

    public void onServerLoginClick() {
        getNavigator().login();
    }

    public void onToRegisterClick() {
        getNavigator().openRegisterActivity();
    }

}
