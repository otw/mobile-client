package com.otw.hermit.ui.main.service;

import com.otw.hermit.data.DataManager;
import com.otw.hermit.utils.rx.SchedulerProvider;

import dagger.Module;
import dagger.Provides;

@Module
public class ServiceModule {

    @Provides
    GeofenceTransitionService provideGeofenceTransitionService(DataManager dataManager, SchedulerProvider schedulerProvider) {
        return new GeofenceTransitionService(dataManager, schedulerProvider);
    }
}
