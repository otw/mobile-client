package com.otw.hermit.ui.main.event;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class EventFragmentProvider {

    @ContributesAndroidInjector(modules = EventFragmentModule.class)
    abstract EventFragment provideEventFragmentFactory();
}
