package com.otw.hermit.ui.scanqr.scannedcontact;

import android.databinding.ObservableField;

import com.otw.hermit.data.model.api.ConnectionsResponse;

public class ScannedContactItemViewModel {

    public final ObservableField<Long> id;

    public final ObservableField<String> name;

    public final ScannedContactItemViewModelListener mListener;

    private final ConnectionsResponse.Contact mContact;

    public ScannedContactItemViewModel(ConnectionsResponse.Contact mContact, ScannedContactItemViewModelListener listener) {
        this.mContact = mContact;
        this.mListener = listener;
        id = new ObservableField<>(this.mContact.getId());
        name = new ObservableField<>(this.mContact.getFirstName() + " " + this.mContact.getLastName());
    }

    public void connect() {
        mListener.connect(mContact);
    }

    public void dontconnect() {
        mListener.dontconnect(mContact);
    }

    public interface ScannedContactItemViewModelListener {
        void connect(ConnectionsResponse.Contact contact);

        void dontconnect(ConnectionsResponse.Contact contact);
    }
}
