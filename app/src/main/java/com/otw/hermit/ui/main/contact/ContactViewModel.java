package com.otw.hermit.ui.main.contact;

import android.arch.lifecycle.MutableLiveData;
import android.databinding.ObservableArrayList;
import android.databinding.ObservableList;

import com.otw.hermit.data.DataManager;
import com.otw.hermit.data.model.api.ConnectionsResponse;
import com.otw.hermit.ui.base.BaseViewModel;
import com.otw.hermit.utils.rx.SchedulerProvider;

import java.util.List;

public class ContactViewModel extends BaseViewModel<ContactNavigator> {

    public final ObservableList<ConnectionsResponse.Contact> contactObservableArrayList = new ObservableArrayList<>();

    private final MutableLiveData<List<ConnectionsResponse.Contact>> contactListLiveData;

    public ContactViewModel(DataManager dataManager,
                            SchedulerProvider schedulerProvider) {
        super(dataManager, schedulerProvider);
        contactListLiveData = new MutableLiveData<>();
        fetchContacts();
    }

    public void addContactItemsToList(List<ConnectionsResponse.Contact> contacts) {
        contactObservableArrayList.clear();
        contactObservableArrayList.addAll(contacts);
    }

    public void fetchContacts() {
        setIsLoading(true);
        getCompositeDisposable().add(getDataManager()
                .serverConnectedContactsCall()
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(response -> {
                    if (response != null && response.isEmpty() == false) {
                        contactListLiveData.setValue(response);
                    }
                    setIsLoading(false);
                }, throwable -> {
                    setIsLoading(false);
                    getNavigator().handleError(throwable);
                }));
    }

    public MutableLiveData<List<ConnectionsResponse.Contact>> getContactListLiveData() {
        return contactListLiveData;
    }

    public ObservableList<ConnectionsResponse.Contact> getContactObservableList() {
        return contactObservableArrayList;
    }
}
