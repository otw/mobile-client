package com.otw.hermit.ui.main.dossier;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class DossierFragmentProvider {

    @ContributesAndroidInjector(modules = DossierFragmentModule.class)
    abstract DossierFragment provideAboutFragmentFactory();

}
