package com.otw.hermit.ui.scanqr;

public interface ScanqrNavigator {

    void handleError(Throwable throwable);

    void openMainActivity();

}
