package com.otw.hermit.ui.main.contact;

import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.view.View;

import com.otw.hermit.BR;
import com.otw.hermit.R;
import com.otw.hermit.ViewModelProviderFactory;
import com.otw.hermit.data.model.api.ConnectionsResponse;
import com.otw.hermit.databinding.FragmentContactBinding;
import com.otw.hermit.ui.base.BaseFragment;
import com.otw.hermit.ui.main.dossier.DossierFragment;

import java.util.List;

import javax.inject.Inject;

import dagger.android.AndroidInjector;
import dagger.android.DispatchingAndroidInjector;
import dagger.android.support.HasSupportFragmentInjector;

public class ContactFragment extends BaseFragment<FragmentContactBinding, ContactViewModel>
        implements ContactNavigator, ContactAdapter.ContactAdapterListener, HasSupportFragmentInjector {

    @Inject
    DispatchingAndroidInjector<Fragment> fragmentDispatchingAndroidInjector;
    @Inject
    ContactAdapter mContactAdapter;
    FragmentContactBinding mFragmentContactBinding;
    @Inject
    LinearLayoutManager mLayoutManager;
    @Inject
    ViewModelProviderFactory mViewModelFactory;
    private ContactViewModel mContactViewModel;

    public static ContactFragment newInstance() {
        Bundle args = new Bundle();
        ContactFragment fragment = new ContactFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public int getBindingVariable() {
        return BR.viewModel;
    }

    @Override
    public int getLayoutId() {
        return R.layout.fragment_contact;
    }

    @Override
    public ContactViewModel getViewModel() {
        mContactViewModel = ViewModelProviders.of(this, mViewModelFactory).get(ContactViewModel.class);
        return mContactViewModel;
    }

    @Override
    public void handleError(Throwable throwable) {
        // handle error
    }

    @Override
    public void updateConnections(List<ConnectionsResponse.Contact> contactList) {
        mContactAdapter.addItems(contactList);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContactViewModel.setNavigator(this);
        mContactAdapter.setListener(this);
    }

    @Override
    public void contactItemClick(ConnectionsResponse.Contact contact) {
        Bundle bundle = new Bundle();
        bundle.putLong("Contact-id", contact.getId());
        bundle.putString("Contact-email", contact.getEmail());
        DossierFragment dossierFragment = DossierFragment.newInstance();
        dossierFragment.setArguments(bundle);

        getActivity().getSupportFragmentManager()
                .beginTransaction()
                .addToBackStack(DossierFragment.TAG)
                .add(R.id.clRootView, dossierFragment, DossierFragment.TAG)
                .commit();
    }

    @Override
    public void onRetryClick() {
        mContactViewModel.fetchContacts();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mFragmentContactBinding = getViewDataBinding();
        setUp();
        subscribeToLiveData();
    }

    private void setUp() {
        mLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        mFragmentContactBinding.contactRecyclerView.setLayoutManager(mLayoutManager);
        mFragmentContactBinding.contactRecyclerView.setItemAnimator(new DefaultItemAnimator());
        mFragmentContactBinding.contactRecyclerView.setAdapter(mContactAdapter);
    }

    private void subscribeToLiveData() {
        mContactViewModel.getContactListLiveData().observe(this, contacts -> mContactViewModel.addContactItemsToList(contacts));
    }

    @Override
    public AndroidInjector<Fragment> supportFragmentInjector() {
        return fragmentDispatchingAndroidInjector;
    }
}
