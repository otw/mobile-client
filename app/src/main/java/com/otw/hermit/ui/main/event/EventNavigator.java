package com.otw.hermit.ui.main.event;

import com.otw.hermit.data.model.api.ConnectionsResponse;

import java.util.List;

public interface EventNavigator {

    void handleError(Throwable throwable);

    void updateConnections(List<ConnectionsResponse.Event> eventList);
}
