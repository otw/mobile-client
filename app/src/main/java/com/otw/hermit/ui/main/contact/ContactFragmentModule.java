package com.otw.hermit.ui.main.contact;

import android.support.v7.widget.LinearLayoutManager;

import com.otw.hermit.data.DataManager;
import com.otw.hermit.utils.rx.SchedulerProvider;

import java.util.ArrayList;

import dagger.Module;
import dagger.Provides;

@Module
public class ContactFragmentModule {

    @Provides
    ContactViewModel contactViewModel(DataManager dataManager,
                                      SchedulerProvider schedulerProvider) {
        return new ContactViewModel(dataManager, schedulerProvider);
    }

    @Provides
    ContactAdapter provideContactAdapter() {
        return new ContactAdapter(new ArrayList<>());
    }

    @Provides
    LinearLayoutManager provideLinearLayoutManager(ContactFragment fragment) {
        return new LinearLayoutManager(fragment.getActivity());
    }
}
