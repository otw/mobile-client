package com.otw.hermit.ui.main.contact;

import com.otw.hermit.data.model.api.ConnectionsResponse;

import java.util.List;

public interface ContactNavigator {

    void handleError(Throwable throwable);

    void updateConnections(List<ConnectionsResponse.Contact> contactList);
}
