package com.otw.hermit.ui.main.dossier;

import android.arch.lifecycle.MutableLiveData;
import android.content.Intent;
import android.databinding.ObservableArrayList;
import android.databinding.ObservableField;
import android.databinding.ObservableList;
import android.net.Uri;

import com.otw.hermit.data.DataManager;
import com.otw.hermit.data.model.api.ConnectionsResponse;
import com.otw.hermit.ui.base.BaseViewModel;
import com.otw.hermit.utils.rx.SchedulerProvider;

import java.util.ArrayList;
import java.util.List;

public class DossierViewModel extends BaseViewModel<DossierNavigator> {

    private final ObservableList<DossierItemViewModel> dossierItemViewModels = new ObservableArrayList<>();
    private final MutableLiveData<List<DossierItemViewModel>> dossierItemsLiveData;
    public ObservableField<Long> id = new ObservableField<>();
    public ObservableField<String> name = new ObservableField<>();
    public ObservableField<String> phoneNumber = new ObservableField<>();
    public ObservableField<String> email = new ObservableField<>();
    public ObservableField<String> description = new ObservableField<>();
    private MutableLiveData<ConnectionsResponse.Contact> contactLiveData;


    public DossierViewModel(DataManager dataManager, SchedulerProvider schedulerProvider) {
        super(dataManager, schedulerProvider);
        contactLiveData = new MutableLiveData<>();
        dossierItemsLiveData = new MutableLiveData<>();
    }

    public void onNavBackClick() {
        getNavigator().goBack();
    }

    public void addContact(ConnectionsResponse.Contact contact) {
        id.set(contact.getId());
        name.set(contact.getFirstName() + " " + contact.getLastName());
        phoneNumber.set(contact.getPhoneNumber());
        email.set(contact.getEmail());
        description.set(contact.getDescription());

        id.notifyChange();
        name.notifyChange();
        phoneNumber.notifyChange();
        email.notifyChange();
        description.notifyChange();
    }

    public void addDossierItemsToList(List<DossierItemViewModel> dossierItems) {
        dossierItemViewModels.clear();
        dossierItemViewModels.addAll(dossierItems);
    }

    public void getDataFromId(Long id) {
        setIsLoading(true);
        getCompositeDisposable().add(getDataManager()
                .serverContactInfoCall(id)
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(response -> {
                    if (response != null) {
                        contactLiveData.setValue(response);
                    }
                    setIsLoading(false);
                }, throwable -> {
                    setIsLoading(false);
                    getNavigator().handleError(throwable);
                }));

        fetchFiles(id);
    }

    public void fetchFiles(Long id) {
        setIsLoading(true);
        getCompositeDisposable().add(getDataManager()
                .serverDossierCall(id)
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(response -> {
                    if (response != null && response.getFile() != null) {
                        dossierItemsLiveData.setValue(getViewModelList(response.getFile()));
                    }
                    setIsLoading(false);
                }, throwable -> {
                    setIsLoading(false);
                    getNavigator().handleError(throwable);
                }));
    }

    public ObservableList<DossierItemViewModel> getDossierItemViewModels() {
        return dossierItemViewModels;
    }

    public MutableLiveData<ConnectionsResponse.Contact> getContactLiveData() {
        return contactLiveData;
    }

    public MutableLiveData<List<DossierItemViewModel>> getDossierFiles() {
        return dossierItemsLiveData;
    }

    public List<DossierItemViewModel> getViewModelList(List<ConnectionsResponse.File> fileList) {
        List<DossierItemViewModel> dossierItemViewModels = new ArrayList<>();
        for (ConnectionsResponse.File file : fileList) {
            dossierItemViewModels.add(new DossierItemViewModel(file));
        }
        return dossierItemViewModels;
    }

    public void makeCall() {
        Intent intent = new Intent(Intent.ACTION_DIAL);
        intent.setData(Uri.parse("tel:" + phoneNumber.get()));
        getNavigator().makeIntent(intent);
    }

    public void makeMessage() {
        Intent sendIntent = new Intent(Intent.ACTION_VIEW);
        sendIntent.setData(Uri.parse("sms:" + phoneNumber.get()));
        sendIntent.putExtra("sms_body", "");
        getNavigator().makeIntent(sendIntent);
    }
}
