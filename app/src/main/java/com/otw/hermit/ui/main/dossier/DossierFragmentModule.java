package com.otw.hermit.ui.main.dossier;

import android.support.v7.widget.LinearLayoutManager;

import com.otw.hermit.data.DataManager;
import com.otw.hermit.utils.rx.SchedulerProvider;

import dagger.Module;
import dagger.Provides;

@Module
public class DossierFragmentModule {

    @Provides
    DossierViewModel dossierViewModel(DataManager dataManager,
                                      SchedulerProvider schedulerProvider) {
        return new DossierViewModel(dataManager, schedulerProvider);
    }

    @Provides
    LinearLayoutManager provideLinearLayoutManager(DossierFragment fragment) {
        return new LinearLayoutManager(fragment.getActivity());
    }

    @Provides
    DossierAdapter provideContactAdapter() {
        return new DossierAdapter();
    }
}
