package com.otw.hermit.ui.main;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.otw.hermit.ui.main.contact.ContactFragment;
import com.otw.hermit.ui.main.event.EventFragment;
import com.otw.hermit.ui.main.potential.PotentialFragment;

public class MainPagerAdapter extends FragmentStatePagerAdapter {

    private int mTabCount;

    public MainPagerAdapter(FragmentManager fragmentManager) {
        super(fragmentManager);
        this.mTabCount = 0;
    }

    @Override
    public int getCount() {
        return mTabCount;
    }

    public void setCount(int count) {
        mTabCount = count;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return ContactFragment.newInstance();
            case 1:
                return EventFragment.newInstance();
            case 2:
                return PotentialFragment.newInstance();
            default:
                return null;
        }
    }
}
