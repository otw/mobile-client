package com.otw.hermit.ui.main.dossier;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.otw.hermit.databinding.ItemFileEmptyViewBinding;
import com.otw.hermit.databinding.ItemFileViewBinding;
import com.otw.hermit.ui.base.BaseViewHolder;

import java.util.ArrayList;
import java.util.List;

public class DossierAdapter extends RecyclerView.Adapter<BaseViewHolder> {

    public static final int VIEW_TYPE_EMPTY = 0;

    public static final int VIEW_TYPE_NORMAL = 1;

    private final List<DossierItemViewModel> mDossierResponseList;

    private DossierAdapterListener mListener;

    public DossierAdapter() {
        this.mDossierResponseList = new ArrayList<>();
    }

    @Override
    public int getItemCount() {
        if (!mDossierResponseList.isEmpty()) {
            return mDossierResponseList.size();
        } else {
            return 1;
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (!mDossierResponseList.isEmpty()) {
            return VIEW_TYPE_NORMAL;
        } else {
            return VIEW_TYPE_EMPTY;
        }
    }

    @Override
    public void onBindViewHolder(BaseViewHolder holder, int position) {
        holder.onBind(position);
    }

    @Override
    public BaseViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        switch (viewType) {
            case VIEW_TYPE_NORMAL:
                ItemFileViewBinding itemFileViewBinding = ItemFileViewBinding.inflate(LayoutInflater.from(parent.getContext()),
                        parent, false);
                return new DossierViewHolder(itemFileViewBinding);
            case VIEW_TYPE_EMPTY:
            default:
                ItemFileEmptyViewBinding emptyViewBinding = ItemFileEmptyViewBinding.inflate(LayoutInflater.from(parent.getContext()),
                        parent, false);
                return new DossierEmptyViewHolder(emptyViewBinding);
        }
    }

    public void addItems(List<DossierItemViewModel> dossierItemViewModels) {
        mDossierResponseList.addAll(dossierItemViewModels);
        notifyDataSetChanged();
    }

    public void clearItems() {
        mDossierResponseList.clear();
    }

    public void setListener(DossierAdapterListener listener) {
        this.mListener = listener;
    }

    public interface DossierAdapterListener {

        void onRetryClick();
    }

    public class DossierViewHolder extends BaseViewHolder implements View.OnClickListener {

        private ItemFileViewBinding mBinding;

        public DossierViewHolder(ItemFileViewBinding binding) {
            super(binding.getRoot());
            this.mBinding = binding;
        }

        @Override
        public void onBind(int position) {
            final DossierItemViewModel mDossierItemViewModel = mDossierResponseList.get(position);
            mBinding.setViewModel(mDossierItemViewModel);


            // Immediate Binding
            // When a variable or observable changes, the binding will be scheduled to change before
            // the next frame. There are times, however, when binding must be executed immediately.
            // To force execution, use the executePendingBindings() method.
            mBinding.executePendingBindings();
        }

        @Override
        public void onClick(View v) {

        }
    }

    public class DossierEmptyViewHolder extends BaseViewHolder implements DossierEmptyItemViewModel.DossierEmptyItemViewModelListener {

        private ItemFileEmptyViewBinding mBinding;

        public DossierEmptyViewHolder(ItemFileEmptyViewBinding binding) {
            super(binding.getRoot());
            this.mBinding = binding;
        }

        @Override
        public void onBind(int position) {
            DossierEmptyItemViewModel emptyItemViewModel = new DossierEmptyItemViewModel(this);
            mBinding.setViewModel(emptyItemViewModel);
        }

        @Override
        public void onRetryClick() {
            mListener.onRetryClick();
        }
    }
}