package com.otw.hermit.ui.main.contact;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class ContactFragmentProvider {

    @ContributesAndroidInjector(modules = ContactFragmentModule.class)
    abstract ContactFragment provideContactFragmentFactory();
}
