package com.otw.hermit.ui.main.dossier;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.view.View;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.otw.hermit.BR;
import com.otw.hermit.R;
import com.otw.hermit.ViewModelProviderFactory;
import com.otw.hermit.databinding.FragmentDossierBinding;
import com.otw.hermit.ui.base.BaseFragment;
import com.otw.hermit.ui.main.GlideApp;

import javax.inject.Inject;

public class DossierFragment extends BaseFragment<FragmentDossierBinding, DossierViewModel> implements DossierNavigator, DossierAdapter.DossierAdapterListener {

    public static final String TAG = DossierFragment.class.getSimpleName();
    @Inject
    LinearLayoutManager mLayoutManager;
    @Inject
    DossierAdapter mDossierAdapter;
    @Inject
    ViewModelProviderFactory factory;
    FragmentDossierBinding mFragmentDossierBinding;
    private DossierViewModel mDossierViewModel;

    public static DossierFragment newInstance() {
        Bundle args = new Bundle();
        DossierFragment fragment = new DossierFragment();
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public int getBindingVariable() {
        return BR.viewModel;
    }

    @Override
    public int getLayoutId() {
        return R.layout.fragment_dossier;
    }

    @Override
    public DossierViewModel getViewModel() {
        mDossierViewModel = ViewModelProviders.of(this, factory).get(DossierViewModel.class);
        return mDossierViewModel;
    }

    @Override
    public void goBack() {
        getBaseActivity().onFragmentDetached(TAG);
    }

    @Override
    public void handleError(Throwable throwable) {

    }

    @Override
    public void makeIntent(Intent intent) {
        startActivity(intent);
    }

    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mFragmentDossierBinding = getViewDataBinding();
        setUp();
        subscribeToLiveData();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mDossierViewModel.setNavigator(this);
        mDossierAdapter.setListener(this);
    }

    private void setUp() {
        mLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        mFragmentDossierBinding.filesRecyclerView.setLayoutManager(mLayoutManager);
        mFragmentDossierBinding.filesRecyclerView.setAdapter(mDossierAdapter);

        Bundle bundle = this.getArguments();
        if (bundle != null) {
            Long id = bundle.getLong("Contact-id", 0L);
            String email = bundle.getString("Contact-email", "");

            mDossierViewModel.getDataFromId(id);

            FirebaseStorage storage = FirebaseStorage.getInstance();
            StorageReference storageRef = storage.getReference();
            StorageReference islandRef = storageRef.child("/contacts/"+email);
            islandRef.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                @Override
                public void onSuccess(Uri uri) {
                    GlideApp.with(mFragmentDossierBinding.profileImage.getContext())
                            .load(uri.toString())
                            .into(mFragmentDossierBinding.profileImage);
                }
            });
        }

    }

    private void subscribeToLiveData() {
        mDossierViewModel.getContactLiveData().observe(this, contact -> mDossierViewModel.addContact(contact));
        mDossierViewModel.getDossierFiles().observe(this, dossierItemViewModels -> mDossierViewModel.addDossierItemsToList(dossierItemViewModels));
    }


    @Override
    public void onRetryClick() {
        Bundle bundle = this.getArguments();
        if (bundle != null) {
            Long id = bundle.getLong("Contact-id", 0L);
            mDossierViewModel.fetchFiles(id);
        }
    }
}