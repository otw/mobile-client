package com.otw.hermit.ui.scanqr.scannedcontact;

import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.otw.hermit.data.model.api.ConnectionsResponse;
import com.otw.hermit.databinding.ItemScannedContactEmptyViewBinding;
import com.otw.hermit.databinding.ItemScannedContactViewBinding;
import com.otw.hermit.ui.base.BaseViewHolder;
import com.otw.hermit.ui.main.GlideApp;

import java.util.List;

public class ScannedContactAdapter extends RecyclerView.Adapter<BaseViewHolder> {

    public static final int VIEW_TYPE_EMPTY = 0;

    public static final int VIEW_TYPE_NORMAL = 1;

    private List<ConnectionsResponse.Contact> mScannedContactResponse;

    private ScannedContactAdapterListener mListener;

    public ScannedContactAdapter(List<ConnectionsResponse.Contact> scannedContactList) {
        this.mScannedContactResponse = scannedContactList;
    }

    @Override
    public int getItemCount() {
        if (mScannedContactResponse != null && mScannedContactResponse.size() > 0) {
            return mScannedContactResponse.size();
        } else {
            return 1;
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (mScannedContactResponse != null && !mScannedContactResponse.isEmpty()) {
            return VIEW_TYPE_NORMAL;
        } else {
            return VIEW_TYPE_EMPTY;
        }
    }

    @Override
    public void onBindViewHolder(BaseViewHolder holder, int position) {
        holder.onBind(position);
    }

    @Override
    public BaseViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        switch (viewType) {
            case VIEW_TYPE_NORMAL:
                ItemScannedContactViewBinding scannedContactViewBinding = ItemScannedContactViewBinding.inflate(LayoutInflater.from(parent.getContext()),
                        parent, false);
                return new ConnectionViewHolder(scannedContactViewBinding);
            case VIEW_TYPE_EMPTY:
            default:
                ItemScannedContactEmptyViewBinding emptyViewBinding = ItemScannedContactEmptyViewBinding.inflate(LayoutInflater.from(parent.getContext()),
                        parent, false);
                return new EmptyViewHolder(emptyViewBinding);
        }
    }

    public void addItems(List<ConnectionsResponse.Contact> responseList) {
        mScannedContactResponse.clear();
        mScannedContactResponse.addAll(responseList);
        notifyDataSetChanged();
    }

    public void setListener(ScannedContactAdapterListener listener) {
        this.mListener = listener;
    }

    public interface ScannedContactAdapterListener {

        void onConnect(ConnectionsResponse.Contact contact);

        void onDontConnect(ConnectionsResponse.Contact contact);
    }

    public class ConnectionViewHolder extends BaseViewHolder implements ScannedContactItemViewModel.ScannedContactItemViewModelListener {

        private ItemScannedContactViewBinding mBinding;

        private ScannedContactItemViewModel mScannedContactItemViewModel;

        public ConnectionViewHolder(ItemScannedContactViewBinding binding) {
            super(binding.getRoot());
            this.mBinding = binding;
        }

        @Override
        public void onBind(int position) {
            final ConnectionsResponse.Contact contact = mScannedContactResponse.get(position);
            mScannedContactItemViewModel = new ScannedContactItemViewModel(contact, this);
            mBinding.setViewModel(mScannedContactItemViewModel);

            FirebaseStorage storage = FirebaseStorage.getInstance();
            StorageReference storageRef = storage.getReference();
            StorageReference islandRef = storageRef.child("/contacts/" + contact.getEmail());
            islandRef.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                @Override
                public void onSuccess(Uri uri) {
                    GlideApp.with(mBinding.profileImage.getContext())
                            .load(uri.toString())
                            .into(mBinding.profileImage);
                }
            });


            // Immediate Binding
            // When a variable or observable changes, the binding will be scheduled to change before
            // the next frame. There are times, however, when binding must be executed immediately.
            // To force execution, use the executePendingBindings() method.
            mBinding.executePendingBindings();
        }

        @Override
        public void connect(ConnectionsResponse.Contact contact) {
            mListener.onConnect(contact);
            mScannedContactResponse.remove(contact);
            notifyDataSetChanged();
        }

        @Override
        public void dontconnect(ConnectionsResponse.Contact contact) {
            mListener.onDontConnect(contact);
            mScannedContactResponse.remove(contact);
            notifyDataSetChanged();
        }
    }

    public class EmptyViewHolder extends BaseViewHolder {

        private ItemScannedContactEmptyViewBinding mBinding;

        public EmptyViewHolder(ItemScannedContactEmptyViewBinding binding) {
            super(binding.getRoot());
            this.mBinding = binding;
        }

        @Override
        public void onBind(int position) {
            ScannedContactEmptyItemViewModel emptyItemViewModel = new ScannedContactEmptyItemViewModel();
            mBinding.setViewModel(emptyItemViewModel);
        }

    }
}