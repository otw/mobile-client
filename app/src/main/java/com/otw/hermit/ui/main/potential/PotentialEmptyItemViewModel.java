package com.otw.hermit.ui.main.potential;

public class PotentialEmptyItemViewModel {

    private EventEmptyItemViewModelListener mListener;

    public PotentialEmptyItemViewModel(EventEmptyItemViewModelListener listener) {
        this.mListener = listener;
    }

    public void onRetryClick() {
        mListener.onRetryClick();
    }

    public interface EventEmptyItemViewModelListener {

        void onRetryClick();
    }
}
