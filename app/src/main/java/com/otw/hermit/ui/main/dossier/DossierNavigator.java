package com.otw.hermit.ui.main.dossier;

import android.content.Intent;

public interface DossierNavigator {

    void goBack();

    void handleError(Throwable throwable);

    void makeIntent(Intent intent);
}
