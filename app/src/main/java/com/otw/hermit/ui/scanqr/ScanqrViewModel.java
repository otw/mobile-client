package com.otw.hermit.ui.scanqr;

import android.arch.lifecycle.MutableLiveData;
import android.databinding.ObservableArrayList;
import android.databinding.ObservableField;
import android.databinding.ObservableList;

import com.otw.hermit.data.DataManager;
import com.otw.hermit.data.model.api.ConnectionsResponse;
import com.otw.hermit.ui.base.BaseViewModel;
import com.otw.hermit.utils.rx.SchedulerProvider;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import io.reactivex.disposables.CompositeDisposable;

import static com.otw.hermit.utils.AppConstants.STANDARD_NOTE;


public class ScanqrViewModel extends BaseViewModel<ScanqrNavigator> {

    public ObservableList<ConnectionsResponse.Contact> contactObservableArrayList = new ObservableArrayList<>();

    private MutableLiveData<List<ConnectionsResponse.Contact>> contactListLiveData;

    private List<ConnectionsResponse.Contact> contacts = new ArrayList<>();

    public final ObservableField<String> eventTitle;

    private List<Long> ids = new LinkedList<>();
    private List<Long> fetchedIds = new LinkedList<>();


    public ScanqrViewModel(DataManager dataManager, SchedulerProvider schedulerProvider) {
        super(dataManager, schedulerProvider);
        contactListLiveData = new MutableLiveData<>();
        eventTitle = new ObservableField<>("Event: -");

        List<Long> events = getDataManager().getCurrentPresentEvents();
        if (!events.isEmpty()) {
            new CompositeDisposable().add(getDataManager()
                    .serverEventInfoCall(events.get(events.size() - 1))
                    .subscribeOn(getSchedulerProvider().io())
                    .observeOn(getSchedulerProvider().ui())
                    .subscribe(response -> {
                        if (response != null) {
                            eventTitle.set("Event: " + response.getTitle());
                        }
                    }, throwable -> {
                    }));
        }
    }

    public void addContactItemsToList(List<ConnectionsResponse.Contact> contacts) {
        contactObservableArrayList.clear();
        contactObservableArrayList.addAll(contacts);
    }

    public void scannedCode(String barcode) {
        String[] barcodeStrings = barcode.split("-");
        if (barcodeStrings[1].equals("HERMIT")) {
            Long connectionId = Long.parseLong(barcodeStrings[0]);
            if (!ids.contains(connectionId)) ids.add(connectionId);

            for (Long id : ids) {
                boolean notInContacts = true;
                for (Long fi : fetchedIds) {
                    if (id.equals(fi)) notInContacts = false;
                }
                if (notInContacts) fetchScannedContacts(id);
            }
        }
    }

    public void fetchScannedContacts(Long id) {
        setIsLoading(true);

        fetchedIds.add(id);

        getCompositeDisposable().add(getDataManager()
                .serverContactInfoCall(id)
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(response -> {
                    if (response != null) {
                        List<ConnectionsResponse.Contact> list = contactListLiveData.getValue();
                        if (list == null) {
                            list = new LinkedList<>();
                        }
                        list.add(response);
                        contactListLiveData.setValue(list);
                    }
                    setIsLoading(false);
                }, throwable -> {
                    setIsLoading(false);
                    getNavigator().handleError(throwable);
                }));

    }

    public MutableLiveData<List<ConnectionsResponse.Contact>> getContactListLiveData() {
        return contactListLiveData;
    }

    public ObservableList<ConnectionsResponse.Contact> getContactObservableList() {
        return contactObservableArrayList;
    }

    public void openMainActivity() {
        getNavigator().openMainActivity();
    }

    public void connectToContact(Long contactId) {
        List<Long> events = getDataManager().getCurrentPresentEvents();
        if (!events.isEmpty()) {
            new CompositeDisposable().add(getDataManager()
                    .serverEventInfoCall(events.get(events.size() - 1))
                    .subscribeOn(getSchedulerProvider().io())
                    .observeOn(getSchedulerProvider().ui())
                    .subscribe(response -> {
                        if (response != null) {
                            ConnectionsResponse.File file = new ConnectionsResponse.File(null, contactId, null, 0.0, 0.0, response.getTitle(), STANDARD_NOTE, response.getId());
                            getDataManager().makeConnectionToContact(contactId, file);
                        }
                    }, throwable -> {
                    }));
        } else {
            ConnectionsResponse.File file = new ConnectionsResponse.File(null, contactId, null, 0.0, 0.0, "Meeting :" + LocalDate.now(), STANDARD_NOTE, null);
            getDataManager().makeConnectionToContact(contactId, file);
        }
    }

    public void dontConnectToContact(ConnectionsResponse.Contact contact) {

    }

}
