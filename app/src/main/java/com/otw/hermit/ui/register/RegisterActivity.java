package com.otw.hermit.ui.register;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;

import com.otw.hermit.BR;
import com.otw.hermit.R;
import com.otw.hermit.databinding.ActivityRegisterBinding;
import com.otw.hermit.ui.base.BaseActivity;
import com.otw.hermit.ui.login.LoginActivity;
import com.otw.hermit.ui.main.MainActivity;

import javax.inject.Inject;

public class RegisterActivity extends BaseActivity<ActivityRegisterBinding, RegisterViewModel> implements RegisterNavigator {

    @Inject
    RegisterViewModel mRegisterViewModel;
    private ActivityRegisterBinding mActivityRegisterBinding;

    public static Intent newIntent(Context context) {
        return new Intent(context, RegisterActivity.class);
    }

    @Override
    public int getBindingVariable() {
        return BR.viewModel;
    }

    @Override
    public int getLayoutId() {
        return R.layout.activity_register;
    }

    @Override
    public RegisterViewModel getViewModel() {
        return mRegisterViewModel;
    }

    @Override
    public void handleError(Throwable throwable) {
        // handle error
    }

    @Override
    public void register() {
        String firstname = mActivityRegisterBinding.etFirstname.getText().toString();
        String lastname = mActivityRegisterBinding.etLastname.getText().toString();
        String email = mActivityRegisterBinding.etEmail.getText().toString();
        String password = mActivityRegisterBinding.etPassword.getText().toString();

        if (mRegisterViewModel.isFirstnameLastnameEmailAndPasswordValid(firstname, lastname, email, password)) {
            hideKeyboard();
            mRegisterViewModel.register(firstname, lastname, email, password);
        } else {
            Toast.makeText(this, getString(R.string.invalid_email_password), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void openMainActivity() {
        Intent intent = MainActivity.newIntent(RegisterActivity.this);
        startActivity(intent);
        finish();
    }

    @Override
    public void openLoginActivity() {
        Intent intent = LoginActivity.newIntent(RegisterActivity.this);
        startActivity(intent);
        finish();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mActivityRegisterBinding = getViewDataBinding();
        mRegisterViewModel.setNavigator(this);
    }
}
