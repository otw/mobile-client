package com.otw.hermit.ui.main.contact;

import android.content.Context;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.otw.hermit.data.model.api.ConnectionsResponse;
import com.otw.hermit.databinding.ItemContactEmptyViewBinding;
import com.otw.hermit.databinding.ItemContactViewBinding;
import com.otw.hermit.ui.base.BaseViewHolder;
import com.otw.hermit.ui.main.GlideApp;

import java.util.List;

import javax.inject.Inject;

public class ContactAdapter extends RecyclerView.Adapter<BaseViewHolder> {

    public static final int VIEW_TYPE_EMPTY = 0;

    public static final int VIEW_TYPE_NORMAL = 1;

    private List<ConnectionsResponse.Contact> mContactResponse;

    private ContactAdapterListener mListener;

    public ContactAdapter(List<ConnectionsResponse.Contact> contactList) {
        this.mContactResponse = contactList;
    }

    @Override
    public int getItemCount() {
        if (mContactResponse != null && mContactResponse.size() > 0) {
            return mContactResponse.size();
        } else {
            return 1;
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (mContactResponse != null && !mContactResponse.isEmpty()) {
            return VIEW_TYPE_NORMAL;
        } else {
            return VIEW_TYPE_EMPTY;
        }
    }

    @Override
    public void onBindViewHolder(BaseViewHolder holder, int position) {
        holder.onBind(position);
    }

    @Override
    public BaseViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        switch (viewType) {
            case VIEW_TYPE_NORMAL:
                ItemContactViewBinding contactViewBinding = ItemContactViewBinding.inflate(LayoutInflater.from(parent.getContext()),
                        parent, false);
                return new ConnectionViewHolder(contactViewBinding);
            case VIEW_TYPE_EMPTY:
            default:
                ItemContactEmptyViewBinding emptyViewBinding = ItemContactEmptyViewBinding.inflate(LayoutInflater.from(parent.getContext()),
                        parent, false);
                return new EmptyViewHolder(emptyViewBinding);
        }
    }

    public void addItems(List<ConnectionsResponse.Contact> responseList) {
        mContactResponse.addAll(responseList);
        notifyDataSetChanged();
    }

    public void clearItems() {
        mContactResponse.clear();
    }

    public void setListener(ContactAdapterListener listener) {
        this.mListener = listener;
    }

    public interface ContactAdapterListener {

        void contactItemClick(ConnectionsResponse.Contact contact);

        void onRetryClick();
    }

    public class ConnectionViewHolder extends BaseViewHolder implements ContactItemViewModel.ContactItemViewModelListener {

        private ItemContactViewBinding mBinding;

        @Inject
        Context context;

        private ContactItemViewModel mContactItemViewModel;

        public ConnectionViewHolder(ItemContactViewBinding binding) {
            super(binding.getRoot());
            this.mBinding = binding;
        }


        @Override
        public void onBind(int position) {
            ConnectionsResponse.Contact contact = mContactResponse.get(position);
            FirebaseStorage storage = FirebaseStorage.getInstance();
            StorageReference storageRef = storage.getReference();
            StorageReference islandRef = storageRef.child("/contacts/" + contact.getEmail());

            islandRef.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                @Override
                public void onSuccess(Uri uri) {
                    GlideApp.with(mBinding.profileImage.getContext())
                            .load(uri.toString())
                            .into(mBinding.profileImage);
                }
            });

            mContactItemViewModel = new ContactItemViewModel(contact, this);
            mBinding.setViewModel(mContactItemViewModel);

            // Immediate Binding
            // When a variable or observable changes, the binding will be scheduled to change before
            // the next frame. There are times, however, when binding must be executed immediately.
            // To force execution, use the executePendingBindings() method.
            mBinding.executePendingBindings();
        }

        @Override
        public void onItemClick(ConnectionsResponse.Contact contact) {
            mListener.contactItemClick(contact);
        }
    }

    public class EmptyViewHolder extends BaseViewHolder implements ContactEmptyItemViewModel.ContactEmptyItemViewModelListener {

        private ItemContactEmptyViewBinding mBinding;

        public EmptyViewHolder(ItemContactEmptyViewBinding binding) {
            super(binding.getRoot());
            this.mBinding = binding;
        }

        @Override
        public void onBind(int position) {
            ContactEmptyItemViewModel emptyItemViewModel = new ContactEmptyItemViewModel(this);
            mBinding.setViewModel(emptyItemViewModel);
        }

        @Override
        public void onRetryClick() {
            mListener.onRetryClick();
        }
    }
}