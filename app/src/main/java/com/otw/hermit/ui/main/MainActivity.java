package com.otw.hermit.ui.main;

import android.Manifest;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.graphics.drawable.Animatable;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.Geofence;
import com.google.android.gms.location.GeofencingClient;
import com.google.android.gms.location.GeofencingRequest;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.otw.hermit.BR;
import com.otw.hermit.R;
import com.otw.hermit.ViewModelProviderFactory;
import com.otw.hermit.data.model.api.ConnectionsResponse;
import com.otw.hermit.databinding.ActivityMainBinding;
import com.otw.hermit.ui.base.BaseActivity;
import com.otw.hermit.ui.main.service.GeofenceTransitionService;
import com.otw.hermit.ui.profile.ProfileActivity;
import com.otw.hermit.ui.scanqr.ScanqrActivity;

import java.util.LinkedList;
import java.util.List;

import javax.inject.Inject;

import dagger.android.AndroidInjector;
import dagger.android.DispatchingAndroidInjector;
import dagger.android.support.HasSupportFragmentInjector;

import static com.otw.hermit.utils.AppConstants.GEOFENCE_RADIUS;
import static com.otw.hermit.utils.AppConstants.GEO_DURATION;
import static com.otw.hermit.utils.AppConstants.NOTIFICATION_MSG;
import static com.otw.hermit.utils.AppConstants.REQ_PERMISSION;

public class MainActivity extends BaseActivity<ActivityMainBinding, MainViewModel>
        implements
        MainNavigator,
        HasSupportFragmentInjector {

    private GeofencingClient googleClient;
    private FusedLocationProviderClient mFusedLocationProviderClient;
    private Location lastLocation;

    @Inject
    DispatchingAndroidInjector<Fragment> fragmentDispatchingAndroidInjector;
    @Inject
    ViewModelProviderFactory factory;
    @Inject
    MainPagerAdapter mPagerAdapter;

    private ActivityMainBinding mActivityMainBinding;
    private MainViewModel mMainViewModel;

    private Toolbar mToolbar;
    private FloatingActionButton mFloatingActionButton;

    public static Intent newIntent(Context context) {
        return new Intent(context, MainActivity.class);
    }

    @Override
    public int getBindingVariable() {
        return BR.viewModel;
    }

    @Override
    public int getLayoutId() {
        return R.layout.activity_main;
    }

    @Override
    public MainViewModel getViewModel() {
        mMainViewModel = ViewModelProviders.of(this, factory).get(MainViewModel.class);
        return mMainViewModel;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        getSupportFragmentManager().popBackStack();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Drawable drawable = item.getIcon();
        if (drawable instanceof Animatable) {
            ((Animatable) drawable).start();
        }
        switch (item.getItemId()) {
            case R.id.navScanQr:
                mMainViewModel.scanQr();
                return true;

            case R.id.navOpenProfile:
                mMainViewModel.openProfile();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void handleError(Throwable throwable) {

    }

    @Override
    public void openScanQrActivity() {
        startActivity(ScanqrActivity.newIntent(this));
        finish();
    }

    @Override
    public void openProfileActivity() {
        startActivity(ProfileActivity.newIntent(this));
    }

    private LocationRequest locationRequest;

    @Override
    public AndroidInjector<Fragment> supportFragmentInjector() {
        return fragmentDispatchingAndroidInjector;
    }

    private PendingIntent geoFencePendingIntent;

    // Create a Intent send by the notification
    public static Intent makeNotificationIntent(Context context, String msg) {
        Intent intent = new Intent(context, MainActivity.class);
        intent.putExtra(NOTIFICATION_MSG, msg);
        return intent;
    }

    @Override
    public void onEventButtonClick() {
        mMainViewModel.emptyPrefs();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mActivityMainBinding = getViewDataBinding();
        mMainViewModel.setNavigator(this);
        setUp();
        setUpFab();
        setUpTabLayout();
        createGoogleClient();
        createNotificationChannel();
        mMainViewModel.fetchEvents();
    }

    private void createNotificationChannel() {
        // Create the NotificationChannel, but only on API 26+ because
        // the NotificationChannel class is new and not in the support library
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            CharSequence name = "CHANNEL";
            String description = "CHANNEL";
            int importance = NotificationManager.IMPORTANCE_DEFAULT;
            NotificationChannel channel = new NotificationChannel("HERMIT-CHANNEL", name, importance);
            channel.setDescription(description);
            // Register the channel with the system; you can't change the importance
            // or other notification behaviors after this
            NotificationManager notificationManager = getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(channel);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    private void setUp() {
        mToolbar = mActivityMainBinding.toolbar;
        mFloatingActionButton = mActivityMainBinding.fabConnection;

        setSupportActionBar(mToolbar);

        if (mMainViewModel.isGeofenceEmpty())
            mActivityMainBinding.removeGeofenceButton.setVisibility(View.INVISIBLE);
        mActivityMainBinding.removeGeofenceButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mMainViewModel.emptyPrefs();
                mActivityMainBinding.removeGeofenceButton.setVisibility(View.INVISIBLE);
            }
        });
    }

    private void setUpFab() {
        mFloatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mMainViewModel.scanQr();
            }
        });
    }

    private void setUpTabLayout() {
        mPagerAdapter.setCount(3);
        mActivityMainBinding.mainViewPager.setAdapter(mPagerAdapter);

        mActivityMainBinding.tabLayout.addTab(mActivityMainBinding.tabLayout.newTab().setText(getString(R.string.contact)));
        mActivityMainBinding.tabLayout.addTab(mActivityMainBinding.tabLayout.newTab().setText(getString(R.string.event)));
        mActivityMainBinding.tabLayout.addTab(mActivityMainBinding.tabLayout.newTab().setText(getString(R.string.potential)));


        mActivityMainBinding.mainViewPager.setOffscreenPageLimit(mActivityMainBinding.tabLayout.getTabCount());
        mActivityMainBinding.mainViewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(mActivityMainBinding.tabLayout));

        mActivityMainBinding.tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                mActivityMainBinding.mainViewPager.setCurrentItem(tab.getPosition());
                LinearLayout tabLayout1 = (LinearLayout) ((ViewGroup) mActivityMainBinding.tabLayout.getChildAt(0)).getChildAt(tab.getPosition());
                TextView tabTextView = (TextView) tabLayout1.getChildAt(1);
                tabTextView.setTextAppearance(R.style.SelectedTab);
                tabTextView.setTypeface(Typeface.createFromAsset(getAssets(), "fonts/montserrat/montserrat_bold.ttf"));
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
                LinearLayout tabLayout1 = (LinearLayout) ((ViewGroup) mActivityMainBinding.tabLayout.getChildAt(0)).getChildAt(tab.getPosition());
                TextView tabTextView = (TextView) tabLayout1.getChildAt(1);
                tabTextView.setTextAppearance(R.style.UnSelectedTab);
                tabTextView.setTypeface(Typeface.createFromAsset(getAssets(), "fonts/montserrat/montserrat_regular.ttf"));

            }
        });

        mActivityMainBinding.mainViewPager.setCurrentItem(2);
        mActivityMainBinding.mainViewPager.setCurrentItem(1);
        mActivityMainBinding.mainViewPager.setCurrentItem(0);
    }

    // Create GoogleApiClient instance
    private void createGoogleClient() {
        googleClient = LocationServices.getGeofencingClient(this);
        mFusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this);
        getLastKnownLocation();
    }

    // Get last known location
    private void getLastKnownLocation() {
        if (checkPermission()) {
            mFusedLocationProviderClient.getLastLocation().addOnSuccessListener(this, new OnSuccessListener<Location>() {
                @Override
                public void onSuccess(Location location) {
                    if (location != null) {
                        System.out.println(location.toString());
                        lastLocation = location;
                    }
                }
            });
        } else askPermission();
    }

    // Check for permission to access Location
    private boolean checkPermission() {
        // Ask for permission if it wasn't granted yet
        return (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED);
    }

    // Asks for permission
    private void askPermission() {
        ActivityCompat.requestPermissions(
                this,
                new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                REQ_PERMISSION
        );
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case REQ_PERMISSION: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // Permission granted
                    getLastKnownLocation();

                } else {
                    // Permission denied
                    permissionsDenied();
                }
                break;
            }
        }
    }

    // App cannot work without the permissions
    private void permissionsDenied() {
        System.out.println("permission denied");
    }

    // Create a Geofence
    private Geofence createGeofence(String title, LatLng latLng, float radius, Long duration) {
        System.out.println("GEOFENCE create geofence");
        return new Geofence.Builder()
                .setRequestId(title)
                .setCircularRegion(latLng.latitude, latLng.longitude, radius)
                .setExpirationDuration(duration)
                .setTransitionTypes(Geofence.GEOFENCE_TRANSITION_ENTER
                        | Geofence.GEOFENCE_TRANSITION_EXIT)
                .build();
    }

    // Create a Geofence Request
    private GeofencingRequest createGeofenceRequest(List<Geofence> geofences) {
        return new GeofencingRequest.Builder()
                .setInitialTrigger(GeofencingRequest.INITIAL_TRIGGER_ENTER)
                .addGeofences(geofences)
                .build();
    }

    private PendingIntent createGeofencePendingIntent() {
        System.out.println("GEOFENCE createPendingIntent");
        // Reuse the PendingIntent if we already have it.
        if (geoFencePendingIntent != null) {
            return geoFencePendingIntent;
        }
        Intent intent = new Intent(this, GeofenceTransitionService.class);
        // We use FLAG_UPDATE_CURRENT so that we get the same pending intent back when
        // calling addGeofences() and removeGeofences().
        geoFencePendingIntent = PendingIntent.getService(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        return geoFencePendingIntent;
    }

    // Add the created GeofenceRequest to the device's monitoring list
    private void addGeofence(GeofencingRequest request) {
        System.out.println("GEOFENCE add to list");
        if (checkPermission()) {
            googleClient.addGeofences(request, createGeofencePendingIntent()).addOnSuccessListener(this, new OnSuccessListener<Void>() {
                @Override
                public void onSuccess(Void aVoid) {
                    System.out.println("geofence added");
                }
            }).addOnFailureListener(this, new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    System.out.println(e.toString());
                }
            });
        }
    }

    // Start Geofence creation process
    public void startGeofence(List<ConnectionsResponse.Event> events) {
        List<Geofence> geofences = new LinkedList<>();
        for (ConnectionsResponse.Event event : events) {
            geofences.add(createGeofence(event.getId().toString(), new LatLng(event.getLatitude(), event.getLongitude()), GEOFENCE_RADIUS, GEO_DURATION));
        }
        GeofencingRequest geofenceRequest = createGeofenceRequest(geofences);
        addGeofence(geofenceRequest);
    }
}
