package com.otw.hermit.ui.main.dossier;

import android.databinding.ObservableDouble;
import android.databinding.ObservableField;
import android.databinding.ObservableLong;

import com.otw.hermit.data.model.api.ConnectionsResponse;

public class DossierItemViewModel {

    public final ObservableField<Long> id;

    public final ObservableLong contactId;

//    public final ObservableField<LocalDateTime> time;

    public final ObservableDouble latitude;

    public final ObservableDouble longitude;

    public final ObservableField<String> title;

//    public final ObservableField<String> note;


    public DossierItemViewModel(ConnectionsResponse.File mFile) {
        id = new ObservableField<>(mFile.getId());
        contactId = new ObservableLong(mFile.getContactId());
//        time = new ObservableField<LocalDateTime>(mFile.getTime());
        latitude = new ObservableDouble(mFile.getLatitude());
        longitude = new ObservableDouble(mFile.getLongitude());
        title = new ObservableField<>(mFile.getTitle());
//        note = new ObservableField<>(mFile.getNote());
    }
}

