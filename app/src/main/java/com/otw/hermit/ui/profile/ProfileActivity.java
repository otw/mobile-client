package com.otw.hermit.ui.profile;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.journeyapps.barcodescanner.BarcodeEncoder;
import com.otw.hermit.BR;
import com.otw.hermit.R;
import com.otw.hermit.databinding.ActivityProfileBinding;
import com.otw.hermit.ui.base.BaseActivity;
import com.otw.hermit.ui.login.LoginActivity;
import com.otw.hermit.ui.main.MainActivity;
import com.otw.hermit.utils.ScreenUtils;

import javax.inject.Inject;

public class ProfileActivity extends BaseActivity<ActivityProfileBinding, ProfileViewModel> implements ProfileNavigator {

    @Inject
    ProfileViewModel mProfileViewModel;
    private ActivityProfileBinding mActivityProfileBinding;
    private Toolbar mToolbar;


    public static Intent newIntent(Context context) {
        return new Intent(context, ProfileActivity.class);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        mProfileViewModel.openMainActivity();
    }

    @Override
    public int getBindingVariable() {
        return BR.viewModel;
    }

    @Override
    public int getLayoutId() {
        return R.layout.activity_profile;
    }

    @Override
    public ProfileViewModel getViewModel() {
        return mProfileViewModel;
    }

    @Override
    public void handleError(Throwable throwable) {
        // handle error
    }


    public void saveProfile() {
        String firstname = mActivityProfileBinding.etFirstname.getText().toString();
        String lastname = mActivityProfileBinding.etLastname.getText().toString();
        String email = mActivityProfileBinding.etEmail.getText().toString();
        String phoneNumber = "";
        String description = "";


        if (mProfileViewModel.isFirstnameLastnameAndEmailValid(email, firstname, lastname)) {
            hideKeyboard();
            mProfileViewModel.saveProfile(firstname, lastname, email, phoneNumber, description);
        } else {
            Toast.makeText(this, getString(R.string.invalid_data), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void logout() {
        openLoginActivity();
    }

    @Override
    public void openMainActivity() {
        Intent intent = MainActivity.newIntent(ProfileActivity.this);
        startActivity(intent);
        finish();
    }

    @Override
    public void openLoginActivity() {
        Intent intent = LoginActivity.newIntent(ProfileActivity.this);
        startActivity(intent);
        finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

            case R.id.btnSave:
                saveProfile();
                return true;


            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.profile, menu);
        return true;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mActivityProfileBinding = getViewDataBinding();
        mProfileViewModel.setNavigator(this);
        setUp();
    }

    public void setUp() {
        String firstname = mProfileViewModel.getFirstName();

        mActivityProfileBinding.etFirstname.setText(mProfileViewModel.getFirstName());
        mActivityProfileBinding.etLastname.setText(mProfileViewModel.getLastName());
        mActivityProfileBinding.etEmail.setText(mProfileViewModel.getEmail());
        mActivityProfileBinding.qrImage.setImageBitmap(getQr(150, 150));

        mActivityProfileBinding.qrImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showQr();
            }
        });

        mToolbar = mActivityProfileBinding.toolbar;
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    public Bitmap getQr(int width, int height) {
        try {
            MultiFormatWriter multiFormatWriter = new MultiFormatWriter();
            BitMatrix bitMatrix = null;
            BarcodeEncoder barcodeEncoder = new BarcodeEncoder();
            bitMatrix = multiFormatWriter.encode(mProfileViewModel.getUserId() + "-HERMIT", BarcodeFormat.QR_CODE, width, height);
            return barcodeEncoder.createBitmap(bitMatrix);
        } catch (WriterException e) {
            e.printStackTrace();
        }
        return null;
    }

    public void showQr() {
        Dialog builder = new Dialog(this);
        builder.requestWindowFeature(Window.FEATURE_NO_TITLE);
        builder.getWindow().setBackgroundDrawable(
                new ColorDrawable(android.graphics.Color.TRANSPARENT));
        builder.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialogInterface) {
                //nothing;
            }
        });

        ImageView imageView = new ImageView(this);
        imageView.setImageBitmap(getQr(ScreenUtils.getScreenWidth(getBaseContext()), ScreenUtils.getScreenWidth(getBaseContext())));
        builder.addContentView(imageView, new RelativeLayout.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT));
        builder.show();


    }
}
