package com.otw.hermit.ui.main.dossier;

public class DossierEmptyItemViewModel {

    private DossierEmptyItemViewModelListener mListener;

    public DossierEmptyItemViewModel(DossierEmptyItemViewModelListener listener) {
        this.mListener = listener;
    }

    public void onRetryClick() {
        mListener.onRetryClick();
    }

    public interface DossierEmptyItemViewModelListener {

        void onRetryClick();
    }
}
