package com.otw.hermit.ui.main.potential;

import android.databinding.ObservableField;

import com.otw.hermit.data.model.api.ConnectionsResponse;

public class PotentialItemViewModel {

    public final ObservableField<Long> id;

    public final ObservableField<String> title;

    public final ObservableField<String> description;

    public final ObservableField<Double> latitude;

    public final ObservableField<Double> longitude;

    public final ObservableField<String> address;

    public final ObservableField<String> date;


    public final EventItemViewModelListener mListener;

    private final ConnectionsResponse.Event mEvent;

    public PotentialItemViewModel(ConnectionsResponse.Event mEvent, EventItemViewModelListener listener) {
        this.mEvent = mEvent;
        this.mListener = listener;
        id = new ObservableField<>(this.mEvent.getId());
        title = new ObservableField<>(this.mEvent.getTitle());
        description = new ObservableField<>(this.mEvent.getDescription());
        latitude = new ObservableField<>(this.mEvent.getLatitude());
        longitude = new ObservableField<>(this.mEvent.getLongitude());
        address = new ObservableField<>(this.mEvent.getAddress());
        date = new ObservableField<>(this.mEvent.getStartDate());

    }

    public void onItemClick() {
        mListener.onItemClick(mEvent.getId());
    }

    public interface EventItemViewModelListener {

        void onItemClick(Long contactId);
    }
}
