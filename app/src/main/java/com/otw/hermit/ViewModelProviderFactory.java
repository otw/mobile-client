package com.otw.hermit;

import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProvider;

import com.otw.hermit.data.DataManager;
import com.otw.hermit.ui.login.LoginViewModel;
import com.otw.hermit.ui.main.MainViewModel;
import com.otw.hermit.ui.main.contact.ContactViewModel;
import com.otw.hermit.ui.main.dossier.DossierViewModel;
import com.otw.hermit.ui.main.event.EventViewModel;
import com.otw.hermit.ui.main.potential.PotentialViewModel;
import com.otw.hermit.ui.scanqr.ScanqrViewModel;
import com.otw.hermit.ui.splash.SplashViewModel;
import com.otw.hermit.utils.rx.SchedulerProvider;

import javax.inject.Inject;

/**
 * <p>
 * A provider factory that persists ViewModels {@link ViewModel}.
 * Used if the view model has a parameterized constructor.
 */
public class ViewModelProviderFactory extends ViewModelProvider.NewInstanceFactory {

    private final DataManager dataManager;
    private final SchedulerProvider schedulerProvider;

    @Inject
    public ViewModelProviderFactory(DataManager dataManager,
                                    SchedulerProvider schedulerProvider) {
        this.dataManager = dataManager;
        this.schedulerProvider = schedulerProvider;
    }

    @Override
    public <T extends ViewModel> T create(Class<T> modelClass) {
        if (modelClass.isAssignableFrom(LoginViewModel.class)) {
            //noinspection unchecked
            return (T) new LoginViewModel(dataManager, schedulerProvider);
        } else if (modelClass.isAssignableFrom(MainViewModel.class)) {
            //noinspection unchecked
            return (T) new MainViewModel(dataManager, schedulerProvider);
        } else if (modelClass.isAssignableFrom(ContactViewModel.class)) {
            //noinspection unchecked
            return (T) new ContactViewModel(dataManager, schedulerProvider);
        } else if (modelClass.isAssignableFrom(EventViewModel.class)) {
            //noinspection unchecked
            return (T) new EventViewModel(dataManager, schedulerProvider);
        } else if (modelClass.isAssignableFrom(PotentialViewModel.class)) {
            //noinspection unchecked
            return (T) new PotentialViewModel(dataManager, schedulerProvider);
        } else if (modelClass.isAssignableFrom(DossierViewModel.class)) {
            //noinspection unchecked
            return (T) new DossierViewModel(dataManager, schedulerProvider);
        } else if (modelClass.isAssignableFrom(SplashViewModel.class)) {
            //noinspection unchecked
            return (T) new SplashViewModel(dataManager, schedulerProvider);
        } else if (modelClass.isAssignableFrom(ScanqrViewModel.class)) {
            //noinspection unchecked
            return (T) new ScanqrViewModel(dataManager, schedulerProvider);
        }

        throw new IllegalArgumentException("Unknown ViewModel class: " + modelClass.getName());
    }
}
