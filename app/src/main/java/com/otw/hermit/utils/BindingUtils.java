package com.otw.hermit.utils;

import android.databinding.BindingAdapter;
import android.support.v7.widget.RecyclerView;

import com.otw.hermit.data.model.api.ConnectionsResponse;
import com.otw.hermit.ui.main.contact.ContactAdapter;
import com.otw.hermit.ui.main.dossier.DossierAdapter;
import com.otw.hermit.ui.main.dossier.DossierItemViewModel;
import com.otw.hermit.ui.main.event.EventAdapter;
import com.otw.hermit.ui.main.potential.PotentialAdapter;
import com.otw.hermit.ui.scanqr.scannedcontact.ScannedContactAdapter;

import java.util.List;

public final class BindingUtils {

    private BindingUtils() {
        // This class is not publicly instantiable
    }

    @BindingAdapter({"connectedContactAdapter"})
    public static void addContactItems(RecyclerView recyclerView, List<ConnectionsResponse.Contact> contacts) {
        ContactAdapter adapter = (ContactAdapter) recyclerView.getAdapter();
        if (adapter != null) {
            adapter.clearItems();
            adapter.addItems(contacts);
        }
    }

    @BindingAdapter({"connectedEventAdapter"})
    public static void addConnectedEventItems(RecyclerView recyclerView, List<ConnectionsResponse.Event> events) {
        EventAdapter adapter = (EventAdapter) recyclerView.getAdapter();
        if (adapter != null) {
            adapter.clearItems();
            adapter.addItems(events);
        }
    }

    @BindingAdapter({"contactFileAdapter"})
    public static void addFileItems(RecyclerView recyclerView, List<DossierItemViewModel> files) {
        DossierAdapter adapter = (DossierAdapter) recyclerView.getAdapter();
        if (adapter != null) {
            adapter.clearItems();
            adapter.addItems(files);
        }
    }

    @BindingAdapter({"scannedContactAdapter"})
    public static void addScannedItems(RecyclerView recyclerView, List<ConnectionsResponse.Contact> contacts) {
        ScannedContactAdapter adapter = (ScannedContactAdapter) recyclerView.getAdapter();
        if (adapter != null) {
            adapter.addItems(contacts);
        }
    }

    @BindingAdapter({"potentialAdapter"})
    public static void addEventItems(RecyclerView recyclerView, List<ConnectionsResponse.Event> events) {
        PotentialAdapter adapter = (PotentialAdapter) recyclerView.getAdapter();
        if (adapter != null) {
            adapter.clearItems();
            adapter.addItems(events);
        }
    }


}
