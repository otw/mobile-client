package com.otw.hermit.utils;

public final class AppConstants {

    public static final long NULL_INDEX = -1L;

    public static final String PREF_NAME = "hermit_pref";

    public static final long GEO_DURATION = 60 * 60 * 1000;

    public static final float GEOFENCE_RADIUS = 50000000.0f;

    public static final String NOTIFICATION_MSG = "NOTIFICATION MSG";

    public static final String STANDARD_NOTE = "Standard note... See desktop for further information";

    // Defined in mili seconds.
    // This number in extremely low, and should be used only for debug
    public static final int UPDATE_INTERVAL = 1000;

    public static final int FASTEST_INTERVAL = 900;

    public static final int REQ_PERMISSION = 999;

    private AppConstants() {
        // This utility class is not publicly instantiable
    }
}
